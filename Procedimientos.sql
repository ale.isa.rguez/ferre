create procedure InsertarInventarioCompras
@Folio Varchar(10),@RFC varchar(10), @Fecha date,@Descuento float, @Subtotal Float, @IVA float, @Total float
as insert into InventarioCompras values (@Folio,@RFC,@Fecha,@Descuento,@Subtotal,@IVA,@Total)

GO

create procedure InsertarInventarioComprasDetalle
@Folio varchar(10),@IDProducto varchar(10),@Descripcion varchar(100),@Cantidad float,@Medida varchar(10),@PrecioCompra float,@Importe float
as insert into InventarioComprasDetalle values(@Folio,@IDProducto,@Descripcion,@Cantidad,@Medida,@PrecioCompra,@Importe)
go


create procedure ConsultarCompras
@Folio varchar(10)
 AS SELECT RFC,Fecha,Descuento,Subtotal,IVA,Total
 FROM InventarioCompras
 WHERE Folio =@Folio
 go

 create procedure ConsultarComprasDetalle
@Folio varchar(10)
 AS SELECT IDProducto,Descripcion,Cantidad,Medida,PrecioCompra,Importe
 FROM InventarioComprasDetalle
 WHERE Folio =@Folio
 go

create procedure EliminarInventarioCompras
@Folio varchar(10)
as delete from InventarioCompras where Folio=@Folio
go


create procedure EliminarInventarioComprasDetalle
@Folio varchar(10)
as delete from InventarioComprasDetalle where Folio=@Folio
go



create trigger IncreInventario
on InventarioComprasDetalle
after insert
as
begin  
declare @IDProducto varchar (10), @Cantidad float,@PrecioCompra float
set @IDProducto=(select IDProducto from inserted)
set @Cantidad= (select Cantidad from inserted)
--set @PrecioCompra=(select PrecioCompra from inserted)

  Update Inventario set Stock=Stock+@Cantidad Where IDProducto=@IDProducto
  --exec cambio @IDProducto,@PrecioCompra
end
go


create procedure cambio
@IDProducto varchar(10), @PrecioCompra float
as Update Productos set PrecioCompra =@PrecioCompra where IDProducto=@IDProducto
go


drop procedure  
drop trigger Notificacion

go


create trigger Notificacion
on Inventario 
after Update 
as
begin  
declare @IDProducto varchar (10), @Minimo float,@Stock float, @Descripcion varchar(100),@Mensaje varchar(100)
set @IDProducto=(select IDProducto from inserted)
set @Descripcion=(select Descripcion from Productos Where IDProducto=@IDProducto)
set @Minimo= (select @Minimo from Productos Where IDProducto=@IDProducto)
set @Stock= (select @Stock from Inventario Where IDProducto=@IDProducto)
set @Mensaje=('El Stock de'+@Descripcion+'ha llegado a su minimo')


  if (@Stock <= @Minimo)
    begin
    Print @Mensaje  
    end
end
go

 
Print'She is'
 RAISERROR ('hh', 0, 1) WITH NOWAIT