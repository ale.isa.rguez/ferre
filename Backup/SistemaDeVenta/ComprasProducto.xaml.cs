﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections;
using System.Data.SqlClient;
using System.Data;

namespace SistemaDeVenta
{
    /// <summary>
    /// Lógica de interacción para ComprasProducto.xaml
    /// </summary>
    public partial class ComprasProducto : Window
    {
        public ComprasProducto()
        {
            InitializeComponent();
            Tb_ID.Focus();
            Dp_Fecha.SelectedDate = DateTime.Today;
            Tb_Descuento.Text = "0";
            CargarProveedor();
            CB_Prov.SelectedIndex = 0;
            Img_Eliminar.Visibility = Visibility.Hidden;
           
           
        }
        public Boolean bExiste = false;
        private void Limpiar()
        {
            Tb_ID.Clear();
            CB_Prov.SelectedIndex = -1;
            Tb_Correo.Clear();
            Tb_Descuento.Clear();
            Tb_IDProducto.Clear();
            CB_Producto.SelectedIndex = -1;
            Tb_IVA.Clear();           
            Tb_PrecioComp.Clear();
            Tb_RFC.Clear();
            Tb_Subtotal.Clear();
            Tb_Telefono.Clear();
            Tb_Total.Clear();
            SpE_Cantidad.Clear();
            Dp_Fecha.SelectedDate = DateTime.Today;
            DtG_Compras.Items.Clear();
            DtG_Compras.Items.Refresh();
            Tb_Descuento.Text = "0";
            Tb_Medida.Clear();
            Tb_ID.Focus();
            Img_Guardar.Visibility = Visibility.Visible;
            SpE_Cantidad.IsEnabled = true;
            CB_Producto.IsEnabled = true;
            CB_Prov.IsEnabled = true;
            Tb_PrecioComp.IsEnabled = true;
            Tb_Descuento.IsEnabled = true;
          // Img_Eliminar.Visibility = Visibility.Visible;
        }
        private void CargarProveedor()
        {

            SqlConnection conexion = new SqlConnection();

            conexion.ConnectionString = ReglasClientes.OperacionesProductos.SCadenaConexion;
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter("SELECT Nombre FROM Proveedores", conexion);
            da.Fill(ds, "Proveedores");
            CB_Prov.ItemsSource = (ds.Tables["Proveedores"].DefaultView);
            CB_Prov.DisplayMemberPath = ds.Tables["Proveedores"].Columns["Nombre"].ToString();
        }
        private void CargarProducto()
        {
            try
            {
                SqlConnection conexion = new SqlConnection();
                conexion.ConnectionString = ReglasClientes.OperacionesProductos.SCadenaConexion;
                DataSet ds = new DataSet();
                //SqlDataAdapter da = new SqlDataAdapter("SELECT Descripcion FROM Productos  ", conexion);
                SqlDataAdapter da = new SqlDataAdapter("SELECT dbo.Productos.Descripcion, dbo.Proveedores.RFC FROM  dbo.ProveedorProducto INNER JOIN" +
                    "        dbo.Productos ON dbo.ProveedorProducto.IDProducto = dbo.Productos.IDProducto INNER JOIN" +
                    "                         dbo.Proveedores ON dbo.ProveedorProducto.RFC = dbo.Proveedores.RFC Where Proveedores.RFC = '" + Tb_RFC.Text + "'"+
                     $"and Descripcion LIKE '%{CB_Producto.Text}%' ", conexion);

                da.Fill(ds, "Productos");
                CB_Producto.ItemsSource = (ds.Tables["Productos"].DefaultView);
                CB_Producto.DisplayMemberPath = ds.Tables["Productos"].Columns["Descripcion"].ToString();
                CB_Producto.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }
        private void Imptotal()
        {
            try
            {
                Double ADD = 0;
                foreach (Listas item in DtG_Compras.Items)
                {
                    ADD += Convert.ToDouble(item.Importe);
                }

                double descuento = (Convert.ToDouble(Tb_Descuento.Text) / 100) * ADD;
                Tb_Subtotal.Text = Convert.ToString((ADD) - descuento);
                Tb_IVA.Text = Convert.ToString(Convert.ToDouble(Tb_Subtotal.Text) * 0.16);
                Tb_Total.Text = Convert.ToString(Convert.ToDouble(Tb_Subtotal.Text) + Convert.ToDouble(Tb_IVA.Text));
            }
            catch (Exception ex)
            {
               // MessageBox.Show(ex.ToString());
                MessageBox.Show("Campo de Descuento vacio");
                Tb_Descuento.Text = "0";
            }

        }
        private class Listas
        {

            public String Renglon { get; set; }
            public String Producto { get; set; }
            public String Descripcion { get; set; }
            public String Cantidad { get; set; }
            public String Medida { get; set; }
            public String PrecioC { get; set; }
            public String Importe { get; set; }


        }     

        

        //private void Tb_PrecioCompra_LostFocus(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        Double dImporta = Convert.ToDouble(SpE_Cantidad.Text) * Convert.ToDouble(Tb_PrecioComp.Text);
        //        Listas productos = new Listas
        //        {
        //            Renglon = (DtG_Compras.Items.Count + 1).ToString(),
        //            Producto = Tb_IDProducto.Text,
        //            Descripcion = Tb_NombreProducto.Text,
        //            Cantidad = SpE_Cantidad.Text,
        //            PrecioU = Tb_PrecioComp.Text,
        //            Importe = dImporta.ToString()

        //        };
        //        ReglasClientes.DatosComprasDetalle detalle = new ReglasClientes.DatosComprasDetalle
        //        {
        //            sFolio = Tb_ID.Text,
        //            sIDProducto = Tb_IDProducto.Text,
        //            sDescripcion = Tb_NombreProducto.Text,
        //            dCantidad = Convert.ToDouble(SpE_Cantidad.Text),
        //            dImporte = dImporta

        //        };
        //        ReglasClientes.OperacionesComprasInventario.AgregarRenglon(detalle);

        //        DtG_Compras.Items.Add(productos);
        //        Imptotal();
        //    }

        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.ToString());
        //    }
        //}

        private void Tb_Descuento_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                Imptotal();
            }
            catch
            {
                MessageBox.Show("Campo Decuesto vacio, Introduzca un valor");
                Tb_Descuento.Text = "0";
            }
        }

        private void Tb_ID_LostFocus(object sender, RoutedEventArgs e)
        {
            CB_Prov_LostFocus(sender, e);
            ReglasClientes.DatosCompras datosCompras = new ReglasClientes.DatosCompras()
            {
                sFolio = Tb_ID.Text
            };
            ReglasClientes.DatosComprasDetalle datosDetalle = new ReglasClientes.DatosComprasDetalle()
            {
                sFolio = Tb_ID.Text
            };

            ReglasClientes.OperacionesComprasInventario operacionesCompras = new ReglasClientes.OperacionesComprasInventario();

            if (operacionesCompras.ConsultarCompras(datosCompras, Tb_ID.Text, ref bExiste))
            {
                Tb_RFC.Text = datosCompras.sIDProveedor;
                Dp_Fecha.Text = Convert.ToString(datosCompras.dFecha);
                Tb_Descuento.Text = Convert.ToString(datosCompras.dDescuento);
                Tb_Subtotal.Text = Convert.ToString(datosCompras.dSubtotal);
                Tb_IVA.Text = Convert.ToString(datosCompras.dIVA);
                Tb_Total.Text = Convert.ToString(datosCompras.dTotal);               
                Tb_RFC_LostFocus(sender, e);

                ArrayList ID = new ArrayList();
                ArrayList Descripcion = new ArrayList();
                ArrayList Cantidad = new ArrayList();
                ArrayList Medida = new ArrayList();
                ArrayList PrecioC = new ArrayList();
                ArrayList Importe = new ArrayList();


                if (operacionesCompras.ConsultarComprasDetalle(Tb_ID.Text, ref ID, ref Descripcion, ref Cantidad,ref Medida, ref PrecioC, ref Importe))
                {
                    for (int x = 0; x < ID.Count; x++)
                    {
                        Listas detalListas = new Listas
                        {
                            Renglon = (DtG_Compras.Items.Count + 1).ToString(),
                            Producto = ID[x].ToString(),
                            Descripcion = Descripcion[x].ToString(),
                            Cantidad = Convert.ToString(Cantidad[x]),
                            Medida = Convert.ToString(Medida[x]),
                            PrecioC = Convert.ToString(PrecioC[x]),
                            Importe = Importe[x].ToString()
                        };
                        ReglasClientes.DatosComprasDetalle detalle = new ReglasClientes.DatosComprasDetalle
                        {
                            sFolio = Tb_ID.Text,
                            sIDProducto = detalListas.Producto,
                            sDescripcion = detalListas.Descripcion,
                            dCantidad = Convert.ToDouble(detalListas.Cantidad),
                            sMedida=detalListas.Medida,
                            dPrecio=Convert.ToDouble(detalListas.PrecioC),
                            dImporte = Convert.ToDouble(detalListas.Importe)
                        };
                        ReglasClientes.OperacionesComprasInventario.AgregarRenglon(detalle);
                        DtG_Compras.Items.Add(detalListas);
                    }

                }

               
            }
            if (!bExiste)
            {
                Dp_Fecha.SelectedDate = DateTime.Today;
            }
            else
            {
                Img_Guardar.Visibility = Visibility.Hidden;
                SpE_Cantidad.IsEnabled = false;
                CB_Producto.IsEnabled = false;
                CB_Prov.IsEnabled = false;
                Tb_PrecioComp.IsEnabled = false;
                Tb_Descuento.IsEnabled = false;
                ReglasClientes.DatosProveedor datosProveedor = new ReglasClientes.DatosProveedor
                {
                    sRFC = Tb_RFC.Text
                };
                ReglasClientes.OperacionesProveedor operacionesProveedor = new ReglasClientes.OperacionesProveedor();
                if (operacionesProveedor.ConsultarProveedor(ref datosProveedor, Tb_RFC.Text, ref bExiste))
                {
                    CB_Prov.Text = datosProveedor.sNombre;
                    Tb_Correo.Text = datosProveedor.sCorreo;
                    Tb_Telefono.Text = datosProveedor.sTel;
                }
            }
        }

        private void SpE_Cantidad_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Tb_IDProducto.Text == "" || Tb_PrecioComp.Text.Length==0)
                {
                    MessageBox.Show("Verifique que no existan campos vacios","Advertencia",MessageBoxButton.OK,MessageBoxImage.Warning);
                }
                else
                {
                    Dp_Fecha.SelectedDate = DateTime.Today;

                    Double dImporta = Convert.ToDouble(SpE_Cantidad.Text) * Convert.ToDouble(Tb_PrecioComp.Text);
                    Listas productos = new Listas
                    {
                        Renglon = (DtG_Compras.Items.Count + 1).ToString(),
                        Producto = Tb_IDProducto.Text,
                        Descripcion = CB_Producto.Text,
                        Cantidad = SpE_Cantidad.Text,
                        Medida=Tb_Medida.Text,
                        PrecioC = Tb_PrecioComp.Text,
                        Importe = dImporta.ToString()

                    };
                    ReglasClientes.DatosComprasDetalle detalle = new ReglasClientes.DatosComprasDetalle
                    {
                        sFolio = Tb_ID.Text,
                        sIDProducto = Tb_IDProducto.Text,
                        sDescripcion = CB_Producto.Text,
                        dCantidad = Convert.ToDouble(SpE_Cantidad.Text),
                        sMedida=Tb_Medida.Text,
                        dPrecio=Convert.ToDouble(Tb_PrecioComp.Text),
                        dImporte = dImporta

                    };
                    ReglasClientes.OperacionesComprasInventario.AgregarRenglon(detalle);

                    DtG_Compras.Items.Add(productos);
                    Imptotal();
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void Im_Limpiar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Limpiar();
        }

        private void Tb_ID_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ReglasClientes.DatosCompras datosCompras = new ReglasClientes.DatosCompras()
                {
                    sFolio = Tb_ID.Text
                };
                String sID = Tb_ID.Text;
                ReglasClientes.OperacionesComprasInventario operacionesCompras = new ReglasClientes.OperacionesComprasInventario();
                if (operacionesCompras.Folio(ref sID))
                {
                    Tb_ID.Text = sID;
                }
                else
                {
                    MessageBox.Show(operacionesCompras.sLastError);
                }

            }
            if (e.Key == Key.F1)
            {
                try {
                    BusquedaCompra busqueda = new BusquedaCompra();
                    busqueda.ShowDialog();
                    Tb_ID.Text = Convert.ToString(((DataRowView)busqueda.DataG_BuscarCompra.Items[busqueda.DataG_BuscarCompra.SelectedIndex])[0]);
                }
                catch
                {
                    MessageBox.Show("No se seleccionó ningun resultado", "Advertencia", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        private void Img_Eliminar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                ReglasClientes.DatosCompras datosCompras = new ReglasClientes.DatosCompras()
                {
                    sFolio = Tb_ID.Text
                };
                ReglasClientes.DatosComprasDetalle datosComprasDetalle = new ReglasClientes.DatosComprasDetalle()
                {
                    sFolio = Tb_ID.Text
                };
                ReglasClientes.OperacionesComprasInventario operacionesComprasInventario = new ReglasClientes.OperacionesComprasInventario();
                if (!operacionesComprasInventario.EliminarCompras(datosComprasDetalle, datosCompras))
                {
                    MessageBox.Show(operacionesComprasInventario.sLastError);
                }
                Limpiar();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void Img_Guardar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (Tb_ID.Text.Length == 0)
                {
                    MessageBox.Show("Verifique que no existan campos vacios", "Advertencia", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    ReglasClientes.DatosCompras datosCompras = new ReglasClientes.DatosCompras()
                    {
                        sFolio = Tb_ID.Text,
                        sIDProveedor = Tb_RFC.Text,
                        dFecha = Convert.ToDateTime(Dp_Fecha.Text),
                        dDescuento = Convert.ToDouble(Tb_Descuento.Text),
                        dSubtotal = Convert.ToDouble(Tb_Subtotal.Text),
                        dIVA = Convert.ToDouble(Tb_IVA.Text),
                        dTotal = Convert.ToDouble(Tb_Total.Text),

                    };
                    double Imp = Convert.ToDouble(SpE_Cantidad.Text) * Convert.ToDouble(Tb_PrecioComp.Text);
                    Double Precio = Convert.ToDouble(Tb_PrecioComp.Text);
                    ReglasClientes.DatosComprasDetalle comprasDetalle = new ReglasClientes.DatosComprasDetalle()
                    {
                        sFolio = Tb_ID.Text,
                        sIDProducto = Tb_IDProducto.Text,
                        sDescripcion = CB_Producto.Text,
                        dCantidad = Convert.ToDouble(SpE_Cantidad.Text),
                        sMedida = Tb_Medida.Text,
                        dPrecio = Precio,
                        dImporte = Imp
                    };
                    ReglasClientes.DatosProveedor datosProveedor = new ReglasClientes.DatosProveedor
                    {
                        sNombre = CB_Prov.Text
                    };
                    ReglasClientes.OperacionesComprasInventario operacionesCompras = new ReglasClientes.OperacionesComprasInventario();
                    if (!operacionesCompras.Altacompras(datosCompras, comprasDetalle, datosProveedor))
                    {
                        MessageBox.Show(operacionesCompras.sLastError);
                    }
                    ReglasClientes.OperacionesComprasInventario.detalleCompra.Clear();
                    Limpiar();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString());
            }
        }

        private void Tb_PrecioComp_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                Double dImporta = Convert.ToDouble(SpE_Cantidad.Text) * Convert.ToDouble(Tb_PrecioComp.Text);
                Listas productos = new Listas
                {
                    Renglon = (DtG_Compras.Items.Count + 1).ToString(),
                    Producto = Tb_IDProducto.Text,
                    Descripcion = CB_Producto.Text,
                    Cantidad = SpE_Cantidad.Text,
                    PrecioC = Tb_PrecioComp.Text,
                    Importe = dImporta.ToString()

                };
                ReglasClientes.DatosComprasDetalle detalle = new ReglasClientes.DatosComprasDetalle
                {
                    sFolio = Tb_ID.Text,
                    sIDProducto = Tb_IDProducto.Text,
                    sDescripcion =CB_Producto.Text,
                    dCantidad = Convert.ToDouble(SpE_Cantidad.Text),
                    dPrecio= Convert.ToDouble(Tb_PrecioComp.Text),
                    dImporte = dImporta

                };
                ReglasClientes.OperacionesComprasInventario.AgregarRenglon(detalle);

                DtG_Compras.Items.Add(productos);
                Imptotal();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                MessageBox.Show("Campo de Descuento vacio");
               Tb_Descuento.Text = "0";
            }
        }                     

        private void Tb_RFC_LostFocus(object sender, RoutedEventArgs e)
        {
            //ReglasClientes.DatosProveedor datosProveedor = new ReglasClientes.DatosProveedor
            //{
            //    sRFC = Tb_RFC.Text
            //};
            //ReglasClientes.OperacionesProveedor operacionesProveedor = new ReglasClientes.OperacionesProveedor(@"LAPTOP-B4FB3AHQ\ALE", "Ferreteria", "usuario-genial", "ale123");
            //if (operacionesProveedor.ConsultarProveedor(ref datosProveedor, Tb_RFC.Text, ref bExiste))
            //{
            //    Tb_Nombre.Text = datosProveedor.sNombre;
            //    Tb_Correo.Text = datosProveedor.sCorreo;
            //    Tb_RFC.Text = datosProveedor.sRFC;
            //    Tb_Telefono.Text = datosProveedor.sTel;
            //}

        }

        private void CB_Prov_LostFocus(object sender, RoutedEventArgs e)
        {
            ReglasClientes.DatosProveedor datosProveedor = new ReglasClientes.DatosProveedor
            {
                sNombre=CB_Prov.Text
            };
            ReglasClientes.OperacionesProveedor operacionesProveedor = new ReglasClientes.OperacionesProveedor();
            if (operacionesProveedor.ConsultarProveedorCB(ref datosProveedor, Tb_RFC.Text, ref bExiste))
            {
               
                Tb_Telefono.Text = datosProveedor.sTel;
                Tb_Correo.Text = datosProveedor.sCorreo;
                Tb_RFC.Text = datosProveedor.sRFC;
            }
            CargarProducto();
            bExiste = false;
        }

        private void CB_Producto_LostFocus(object sender, RoutedEventArgs e)
        {
            ReglasClientes.DatosProductos datosProductos = new ReglasClientes.DatosProductos()
            {
               
                sDescripcion=CB_Producto.Text
                
            };
            ReglasClientes.OperacionesProductos operacionesProductos = new ReglasClientes.OperacionesProductos();
            if (operacionesProductos.ConsultarCB(ref datosProductos, Tb_IDProducto.Text, ref bExiste))
            {
                Tb_IDProducto.Text = datosProductos.sIDProducto;
              
               // Tb_PrecioComp.Text = Convert.ToString(datosProductos.dPrecioCompra);
                Tb_Medida.Text = datosProductos.sUnidad;


            }
            SpE_Cantidad.Text = "1";
            bExiste = false;
        }

        private void DtG_Compras_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                switch (sender)
                {
                    case DataGrid grid when grid.Items.Count > 0:
                        int i = grid.SelectedIndex;
                        if (i >= 0 && i < ReglasClientes.OperacionesComprasInventario.detalleCompra.Count)
                        {
                            ReglasClientes.OperacionesComprasInventario.detalleCompra.RemoveAt(i);
                            
                            DtG_Compras.Items.RemoveAt(i);
                            DtG_Compras.Items.Refresh();
                            Imptotal();
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ADVERTENCIA", ex.Message);
                // Advertencia("ADVERTENCIA", ex.Message);
            }
        }

        private void Tb_ID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key >= Key.D0 && e.Key <= Key.D9 || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void Tb_Descuento_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.Key >= Key.D0 && e.Key <= Key.D9 || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9)
            //{
            //    e.Handled = false;
            //}
            //else
            //{
            //    e.Handled = true;
            //}
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        
        private void Tb_PrecioComp_KeyDown(object sender, KeyEventArgs e)
        {           

            if (e.Key >= Key.D0 && e.Key <= Key.D9 || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9|| e.Key == Key.OemPeriod)
            {
                e.Handled = false;
            }            
            else
            {                 
                e.Handled = true;
            }


        }

        private void Tb_IDProducto_LostFocus(object sender, RoutedEventArgs e)
        {
            ReglasClientes.DatosProductos datosProductos = new ReglasClientes.DatosProductos()
            {

             sIDProducto=Tb_IDProducto.Text

            };
            ReglasClientes.OperacionesComprasInventario  operacionesProductos = new ReglasClientes.OperacionesComprasInventario();
            if (operacionesProductos.ConsultarID(ref datosProductos, Tb_IDProducto.Text, ref bExiste))
            {
                CB_Producto.Text = datosProductos.sDescripcion;
                               
                Tb_Medida.Text = datosProductos.sUnidad;


            }
            SpE_Cantidad.Text = "1";
            bExiste = false;
        }

        private void CB_Productos_KeyUp(object sender, KeyEventArgs e)
        {
            if (CB_Producto.Text.Length == 3)
            {
                CargarProducto();
            }
        }
    }
}
