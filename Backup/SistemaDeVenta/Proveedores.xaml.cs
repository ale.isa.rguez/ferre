﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SistemaDeVenta
{
    /// <summary>
    /// Lógica de interacción para Proveedores.xaml
    /// </summary>
    public partial class Proveedores : Window
    {
        Boolean bExiste = false;
       
        public Proveedores()
        {
            InitializeComponent();
            Tb_RFC.Focus();
            Img_Eliminar.Visibility = Visibility.Hidden;
        }
        private void Limpiar()
        {
           
            Tb_Nombre.Clear();
            Tb_Tel.Clear();
            Tb_Email.Clear();
            Tb_RFC.Clear();
            Tb_RFC.Focus();
            bExiste = false;


        }
        
        private void Image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (Tb_RFC.Text.Length == 0 || Tb_Nombre.Text.Length == 0 || Tb_Tel.Text.Length == 0 || Tb_Email.Text.Length == 0)
                {
                    MessageBox.Show("Verifique que no existan campos vacios", "Advertencia", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    ReglasClientes.DatosProveedor datosProveedor = new ReglasClientes.DatosProveedor()
                    {

                        sNombre = Tb_Nombre.Text,
                        sTel = Tb_Tel.Text,
                        sCorreo = Tb_Email.Text,
                        sRFC = Tb_RFC.Text

                    };
                    if (!bExiste)
                    {
                        ReglasClientes.OperacionesProveedor operacionesProveedor = new ReglasClientes.OperacionesProveedor();


                        if (!operacionesProveedor.AltaProveedor(datosProveedor))
                        {
                            MessageBox.Show(operacionesProveedor.sLastError);
                        }

                        bExiste = true;
                    }
                    else
                    {
                        ReglasClientes.OperacionesProveedor operacionesProveedor = new ReglasClientes.OperacionesProveedor();

                        if (!operacionesProveedor.ActvualizarProveedor(datosProveedor))
                        {
                            MessageBox.Show(operacionesProveedor.sLastError);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            Limpiar();
        }

        private void Image_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            Limpiar();
        }

        private void Image_MouseDown_2(object sender, MouseButtonEventArgs e)
        {
            ReglasClientes.DatosProveedor datosProveedor = new ReglasClientes.DatosProveedor()
            {
               sRFC=Tb_RFC.Text          

            };
            ReglasClientes.OperacionesProveedor operacionesProveedor = new ReglasClientes.OperacionesProveedor();
            if (!operacionesProveedor.EliminarProveedor(datosProveedor))
            {
                MessageBox.Show(operacionesProveedor.sLastError);
            }
            Limpiar();
        }

        private void Tb_RFC_LostFocus(object sender, RoutedEventArgs e)
        {
            ReglasClientes.DatosProveedor datosProveedor = new ReglasClientes.DatosProveedor()
            {
                sRFC = Tb_RFC.Text

            };
            ReglasClientes.OperacionesProveedor operacionesProveedor = new ReglasClientes.OperacionesProveedor();
            {

                if (operacionesProveedor.ConsultarProveedor(ref datosProveedor, Tb_RFC.Text, ref bExiste))
                {
                    Tb_Nombre.Text = datosProveedor.sNombre;
                    Tb_Tel.Text = datosProveedor.sTel;
                    Tb_Email.Text = datosProveedor.sCorreo;

                }
                else
                {
                    MessageBox.Show(operacionesProveedor.sLastError);
                }

            }
        }

        private void Tb_Tel_KeyDown(object sender, KeyEventArgs e)
        {
           
            if (e.Key >= Key.D0 && e.Key <= Key.D9 || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Tb_RFC_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                try
                {
                    BusquedaProveedor proveedor = new BusquedaProveedor();
                    proveedor.ShowDialog();
                    Tb_RFC.Text = Convert.ToString(((DataRowView)proveedor.DataG_BuscarPov.Items[proveedor.DataG_BuscarPov.SelectedIndex])[0]);
                }
                catch
                {
                    MessageBox.Show("No se seleccionó ningún resultado", "Advertencia", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }
    }
}
