﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;

namespace SistemaDeVenta
{
    /// <summary>
    /// Lógica de interacción para BusquedaProducto.xaml
    /// </summary>
    public partial class BusquedaProducto : Window
    {
        public BusquedaProducto()
        {
            InitializeComponent();
        }
        private String SCadenaConexion = ReglasClientes.OperacionesProductos.SCadenaConexion;
        public void Buscar()
        {
            using (SqlConnection conn = new SqlConnection(SCadenaConexion))
            {
                conn.Open();
                try
                {

                    string sQry = "SELECT IDProducto,Descripcion " +
                        "FROM Productos " +
                        $"WHERE (Descripcion) LIKE '%{Tb_BuscarCliente.Text}%' ";
                    SqlCommand comando = new SqlCommand(sQry, conn);

                    DataTable tabla = new DataTable();
                    SqlDataAdapter adapter = new SqlDataAdapter(comando);
                    adapter.Fill(tabla);
                    DataG_BuscarCliente.ItemsSource = tabla.DefaultView;
                    conn.Close();
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());

                }
            }
        }
        private void Tb_BuscarCliente_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Buscar();
            }
            if (e.Key == Key.Escape)
            {
                this.Close();
            }
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                
                this.Close();

            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}
