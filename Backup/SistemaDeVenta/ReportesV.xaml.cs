﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SistemaDeVenta
{
    /// <summary>
    /// Lógica de interacción para ReportesV.xaml
    /// </summary>
    public partial class ReportesV : Window
    {
        public ReportesV()
        {
            InitializeComponent();

            Reglas();
        }

        public void Reglas()
        {
            ReglasClientes.ReporteVista report = new ReglasClientes.ReporteVista();
            pivotgrid1.DataSource = report.Vista();
        }

        private void Bt_Imprimir_Click(object sender, RoutedEventArgs e)
        {
           
        }

        private void Img_Print_MouseDown(object sender, MouseButtonEventArgs e)
        {
            String DocumentName = "Preview Document";
            String Title = "Print Preview";
            pivotgrid1.ShowPrintPreview(this, DocumentName, Title);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}
