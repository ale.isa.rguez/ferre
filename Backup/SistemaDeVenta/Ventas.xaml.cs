﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Telerik.Reporting.Processing;

namespace SistemaDeVenta
{
    /// <summary>
    /// Lógica de interacción para Ventas.xaml
    /// </summary>
    public partial class Ventas : Window
    {
        Boolean bExiste = false;

        public Ventas()
        {
            InitializeComponent();
            Tb_ID.Focus();
            Dp_Fecha.SelectedDate = DateTime.Today;

            Tb_Descuento.Text = "0";
          
            Img_Eliminar.Visibility = Visibility.Hidden;
            CmB_Cliente.SelectedIndex = 2;
        }
        List<Listas> Listita = new List<Listas>();
        private void Limpiar()
        {
            Tb_IDUsuario.Clear();
            Tb_NombreUser.Clear();
            Tb_IDProducto.Clear();
            CB_Productos.SelectedIndex = -1;
            Tb_PrecioUnitario.Clear();
            SpE_Cantidad.Clear();
            Tb_ID.Clear();
            Tb_Descuento.Clear();
            Tb_Subtotal.Clear();
            Tb_IVA.Clear();
            Tb_Total.Clear();
            Tb_Medida.Clear();
            Dp_Fecha.SelectedDate = DateTime.Today;
            DtG_Ventas.Items.Clear();
            DtG_Ventas.Items.Refresh();
            Tb_Descuento.Text = "0";
            Tb_ID.Focus();
            Img_Guardar.Visibility = Visibility.Visible;
            CB_Productos.IsEnabled = true;
            SpE_Cantidad.IsEnabled = true;
            Tb_IDProducto.IsEnabled = true;
            Tb_PrecioUnitario.IsEnabled = true;
            Tb_Descuento.IsEnabled = true;

            //Tb_ID.Focus();
            bExiste = false;

        }

        private void CargarProducto()
        {
            try
            {
                SqlConnection conexion = new SqlConnection();
                conexion.ConnectionString = ReglasClientes.OperacionesProductos.SCadenaConexion;
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("SELECT Descripcion FROM Productos  " +
                                $"WHERE (Descripcion) LIKE '%{CB_Productos.Text}%' ", conexion);
                da.Fill(ds, "Productos");
                CB_Productos.ItemsSource = (ds.Tables["Productos"].DefaultView);
                CB_Productos.DisplayMemberPath = ds.Tables["Productos"].Columns["Descripcion"].ToString();
              

               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }
        public void ExportToPDF(Telerik.Reporting.Report toExport, String sFileName)
        {
            try
            {
                ReportProcessor reportProcessor = new ReportProcessor();
                Telerik.Reporting.InstanceReportSource instance = new Telerik.Reporting.InstanceReportSource();
                instance.ReportDocument = toExport;
                RenderingResult result = reportProcessor.RenderReport("PDF", instance, null);
                using (FileStream fs = new FileStream(sFileName, FileMode.Create))
                {
                    fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void Imprimir()
        {
            ReglasClientes.DatosVentas Dventas = new ReglasClientes.DatosVentas
            {
                sFolio = Tb_ID.Text,
                dDescuento = Convert.ToDouble(Tb_Descuento.Text),
                Fecha = Convert.ToDateTime(Dp_Fecha.Text),
                dSubtotal = Convert.ToDouble(Tb_Subtotal.Text),
                dIVA = Convert.ToDouble(Tb_IVA.Text),
                dTotal = Convert.ToDouble(Tb_Total.Text)
            };
            ReglasClientes.OperacionesVentas ventas = new ReglasClientes.OperacionesVentas();
            Report1 report = new Report1(Tb_ID.Text);
            report.ReportParameters["Tb_ID"].Value = Tb_ID.Text;
            report.ReportParameters["Dp_Fecha"].Value = Dp_Fecha.Text;
            report.ReportParameters["Tb_Descuento"].Value = Tb_Descuento.Text;
            report.ReportParameters["Tb_Subtotal"].Value = Tb_Subtotal.Text;
            report.ReportParameters["Tb_IVA"].Value = Tb_IVA.Text;
            report.ReportParameters["Tb_Total"].Value = Tb_Total.Text;

            report.DataSource = ventas.ConsultarVentas(Dventas, Tb_ID.Text, ref bExiste);
            //ExportToPDF(report, @"C:\Users\alejandra\Desktop\SistemaDeVentaCopia - local - 0.5\SistemaDeVenta\bin\Debug\Ticket.PDF");
            ExportToPDF(report, @"C:\SistemaDeVenta\bin\Debug\Ticket.PDF");


            PrintDocument document = new PrintDocument();
            //document.PrintPage += new PrintPageEventHandler(report.printDocument1_PrintPage);
            //document.Print();






        }
        private class Listas
        {

            public String Renglon { get; set; }
            public String Producto { get; set; }
            public String Descripcion { get; set; }
            public String Cantidad { get; set; }
            public String Medida { get; set; }
            public String PrecioU { get; set; }
            public String Importe { get; set; }


        }


        private void Tb_IDProducto_LostFocus(object sender, RoutedEventArgs e)
        {
            ReglasClientes.DatosProductos datosProductos = new ReglasClientes.DatosProductos()
            {
                sIDProducto = Tb_IDProducto.Text
            };
            ReglasClientes.OperacionesProductos operacionesProductos = new ReglasClientes.OperacionesProductos();
            if (operacionesProductos.Consultar(ref datosProductos, Tb_IDProducto.Text, ref bExiste))
            {
                Tb_IDProducto.Text = datosProductos.sIDProducto;
                if (CmB_Cliente.Text == "Mayoreo")
                {
                    Tb_PrecioUnitario.Text = Convert.ToString(datosProductos.dPrecioMayoreo);
                }
                else if (CmB_Cliente.Text == "Frecuente")
                {
                    Tb_PrecioUnitario.Text = Convert.ToString(datosProductos.dPrecioFrecuente);
                }
                else
                {
                    Tb_PrecioUnitario.Text = Convert.ToString(datosProductos.dPrecioPublico);
                }


            }

        }

        private void Tb_PrecioUnitario_LostFocus(object sender, RoutedEventArgs e)
        {
            //    try
            //    {               
            //        Double dImporta = Convert.ToDouble(SpE_Cantidad.Text) * Convert.ToDouble(Tb_PrecioUnitario.Text);                
            //        Listas productos = new Listas
            //        {
            //            Renglon = (DtG_Ventas.Items.Count + 1).ToString(),
            //            Producto = Tb_IDProducto.Text,
            //            Descripcion = CB_Productos.Text,
            //            Cantidad = SpE_Cantidad.Text,
            //            PrecioU = Tb_PrecioUnitario.Text,
            //            Importe = dImporta.ToString()

            //        };
            //        ReglasClientes.DatosVentasDetalle detalle = new ReglasClientes.DatosVentasDetalle
            //        {
            //         sFolio=Tb_ID.Text,
            //         sIDProducto=Tb_IDProducto.Text,
            //         sDescripcion=CB_Productos.Text,
            //         dCantidad= Convert.ToDouble(SpE_Cantidad.Text),
            //         dImporte=dImporta

            //        };
            //        ReglasClientes.OperacionesVentas.AgregarRenglon(detalle);

            //        DtG_Ventas.Items.Add(productos);
            //        Imptotal();

            //    }
            //    catch (Exception ex)
            //    {
            //        MessageBox.Show(ex.ToString());
            //    }

        }


        private void Im_Limpiar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Limpiar();
        }

        private void Img_Guardar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (Tb_ID.Text.Length == 0 || Tb_Descuento.Text.Length == 0 || Tb_Subtotal.Text.Length == 0 || Tb_IVA.Text.Length == 0 || Tb_Total.Text.Length == 0 || DtG_Ventas.Items.Count == 0)
                {
                    MessageBox.Show("Verifique que no existan campos vacios", "Advertencia", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    ReglasClientes.DatosVentas datosVentas = new ReglasClientes.DatosVentas()
                    {
                        sFolio = Tb_ID.Text,
                        sIDUsuario = Tb_IDUsuario_Copy.Text,
                        Fecha = Convert.ToDateTime(Dp_Fecha.Text),
                        dDescuento = Convert.ToDouble(Tb_Descuento.Text),
                        dSubtotal = Convert.ToDouble(Tb_Subtotal.Text),
                        dIVA = Convert.ToDouble(Tb_IVA.Text),
                        dTotal = Convert.ToDouble(Tb_Total.Text),

                    };
                    double Imp = Convert.ToDouble(SpE_Cantidad.Text) * Convert.ToDouble(Tb_PrecioUnitario.Text);
                    ReglasClientes.DatosVentasDetalle ventasDetalle = new ReglasClientes.DatosVentasDetalle()
                    {
                        sFolio = Tb_ID.Text,
                        sIDProducto = Tb_IDProducto.Text,
                        sDescripcion = CB_Productos.Text,
                        dCantidad = Convert.ToDouble(SpE_Cantidad.Text),
                        sMedida = Tb_Medida.Text,
                        dPrecioUnitario = Convert.ToDouble(Tb_PrecioUnitario.Text),
                        dImporte = Imp
                    };
                    ReglasClientes.DatosInventario inv = new ReglasClientes.DatosInventario() { };
                    ReglasClientes.DatosProductos Pr = new ReglasClientes.DatosProductos() { };

                    ReglasClientes.OperacionesVentas operacionesVentas = new ReglasClientes.OperacionesVentas();
                    if (operacionesVentas.AltaVentas(datosVentas, ventasDetalle, inv, Pr))
                    {
                        Imprimir();
                        Report1 R = new Report1(Tb_ID.Text);
                        R.PrintReport(sender, e);
                        ReglasClientes.OperacionesVentas.detalleVenta.Clear();
                    }
                    else
                    {
                        MessageBox.Show(operacionesVentas.sLastError);
                    }

                    Limpiar();

                    
                    ArrayList Stock = new ArrayList();
                    ArrayList Minimo = new ArrayList();
                    ArrayList IDP = new ArrayList();
                    if (operacionesVentas.Notificacion(ventasDetalle, ref Stock, ref Minimo,ref IDP))
                    {
                       
                        for (int x = 0; x < IDP.Count; x++)
                         { 
                        
                           if (Convert.ToDouble(Stock[x]) <= Convert.ToDouble(Minimo[x]))
                           {
                            MessageBox.Show("El Stock de su producto con código "+ IDP[x] +", ha llegado  su mínimo.", "Advertencia", MessageBoxButton.OK, MessageBoxImage.Warning);
                           }
                        }
                    }

                  

                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString());
            }
        }

        private void Img_Eliminar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ReglasClientes.DatosVentas datosVentas = new ReglasClientes.DatosVentas()
            {
                sFolio = Tb_ID.Text
            };
            ReglasClientes.OperacionesVentas operacionesVentas = new ReglasClientes.OperacionesVentas();
            if (!operacionesVentas.EliminarVentas(datosVentas))
            {
                MessageBox.Show(operacionesVentas.sLastError);
            }
            Limpiar();

        }

        private void Tb_Descuento_LostFocus(object sender, RoutedEventArgs e)
        {


            Imptotal();


        }
        private void Imptotal()
        {
            try
            {
                Double ADD = 0;
                foreach (Listas item in DtG_Ventas.Items)
                {
                    ADD += Convert.ToDouble(item.Importe);
                }

                double descuento = (Convert.ToDouble(Tb_Descuento.Text) / 100) * ADD;
                Tb_Subtotal.Text = Convert.ToString(Math.Round(((ADD) - descuento),2));
                Tb_IVA.Text = Convert.ToString(Math.Round(Convert.ToDouble(Tb_Subtotal.Text) * 0.16,2));
                Tb_Total.Text = Convert.ToString(Math.Round(Convert.ToDouble(Tb_Subtotal.Text) + Convert.ToDouble(Tb_IVA.Text),2));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }
        private void Tb_ID_LostFocus(object sender, RoutedEventArgs e)
        {

            ReglasClientes.DatosVentas datosVentas = new ReglasClientes.DatosVentas()
            {
                sFolio = Tb_ID.Text
            };
            ReglasClientes.DatosVentasDetalle datosDetalle = new ReglasClientes.DatosVentasDetalle()
            {
                sFolio = Tb_ID.Text
            };

            ReglasClientes.OperacionesVentas operacionesVentas = new ReglasClientes.OperacionesVentas();

            if (operacionesVentas.ConsultarVentas(datosVentas, Tb_ID.Text, ref bExiste))
            {
                Tb_IDUsuario.Text = datosVentas.sIDUsuario;
                Dp_Fecha.Text = Convert.ToString(datosVentas.Fecha);
                Tb_Descuento.Text = Convert.ToString(datosVentas.dDescuento);
                Tb_Subtotal.Text = Convert.ToString(datosVentas.dSubtotal);
                Tb_IVA.Text = Convert.ToString(datosVentas.dIVA);
                Tb_Total.Text = Convert.ToString(datosVentas.dTotal);
                if (bExiste)
                {

                    Img_Guardar.Visibility = Visibility.Hidden;
                    CB_Productos.IsEnabled = false;
                    SpE_Cantidad.IsEnabled = false;
                    Tb_IDProducto.IsEnabled = false;
                    Tb_PrecioUnitario.IsEnabled = false;
                    Tb_Descuento.IsEnabled = false;

                }
                else
                {
                    Dp_Fecha.SelectedDate = DateTime.Today;
                }

                Tb_IDUsuario_LostFocus(sender, e);

                ArrayList ID = new ArrayList();
                ArrayList Descripcion = new ArrayList();
                ArrayList Cantidad = new ArrayList();
                ArrayList Medida = new ArrayList();
                ArrayList PrecioU = new ArrayList();
                ArrayList Importe = new ArrayList();


                if (operacionesVentas.ConsultarVentasDetalle(Tb_ID.Text, ref ID, ref Descripcion, ref Cantidad, ref Medida, ref PrecioU, ref Importe, ref bExiste))
                {
                    for (int x = 0; x < ID.Count; x++)
                    {
                        Listas detalListas = new Listas
                        {
                            Renglon = (DtG_Ventas.Items.Count + 1).ToString(),
                            Producto = ID[x].ToString(),
                            Descripcion = Descripcion[x].ToString(),
                            Cantidad = Convert.ToString(Cantidad[x]),
                            Medida = Medida[x].ToString(),
                            PrecioU = Convert.ToString(PrecioU[x]),
                            Importe = Importe[x].ToString()
                        };
                        ReglasClientes.DatosVentasDetalle detalle = new ReglasClientes.DatosVentasDetalle
                        {
                            sFolio = Tb_ID.Text,
                            sIDProducto = detalListas.Producto,
                            sDescripcion = detalListas.Descripcion,
                            dCantidad = Convert.ToDouble(detalListas.Cantidad),
                            sMedida = Tb_Medida.Text,
                            dImporte = Convert.ToDouble(detalListas.Importe)
                        };
                        ReglasClientes.OperacionesVentas.AgregarRenglon(detalle);
                        DtG_Ventas.Items.Add(detalListas);
                    }

                }



            }


        }

        private void Tb_ID_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Enter)
            {
                ReglasClientes.DatosVentas datosVentas = new ReglasClientes.DatosVentas()
                {
                    sFolio = Tb_ID.Text
                };
                String sID = Tb_ID.Text;
                ReglasClientes.OperacionesVentas operacionesVentas = new ReglasClientes.OperacionesVentas();
                if (operacionesVentas.Folio(ref sID))
                {
                    Tb_ID.Text = sID;
                }
                else
                {
                    MessageBox.Show(operacionesVentas.sLastError);
                }
            }
            if (e.Key == Key.F1)
            {
                try
                {
                    BuscarVenta buscarVenta = new BuscarVenta();
                    buscarVenta.ShowDialog();
                    Tb_ID.Text = Convert.ToString(((DataRowView)buscarVenta.DataG_BuscarCliente.Items[buscarVenta.DataG_BuscarCliente.SelectedIndex])[0]);
                }
                catch
                {
                    MessageBox.Show("No se seleccionó ningún resultado", "Advertencia", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            if (e.Key >= Key.D0 && e.Key <= Key.D9 || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }

        }

        private void SpE_Cantidad_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {

                if (Tb_IDProducto.Text == string.Empty)
                {
                    MessageBox.Show("Seleccione un Producto", "Advertencia", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                    Dp_Fecha.SelectedDate = DateTime.Today;

                Double dImporta = Convert.ToDouble(SpE_Cantidad.Text) * Convert.ToDouble(Tb_PrecioUnitario.Text);
                Listas productos = new Listas
                {
                    Renglon = (DtG_Ventas.Items.Count + 1).ToString(),
                    Producto = Tb_IDProducto.Text,
                    Descripcion = CB_Productos.Text,
                    Cantidad = SpE_Cantidad.Text,
                    Medida = Tb_Medida.Text,
                    PrecioU = Tb_PrecioUnitario.Text,
                    Importe = dImporta.ToString()

                };
                ReglasClientes.DatosVentasDetalle detalle = new ReglasClientes.DatosVentasDetalle
                {
                    sFolio = Tb_ID.Text,
                    sIDProducto = Tb_IDProducto.Text,
                    sDescripcion = CB_Productos.Text,
                    dCantidad = Convert.ToDouble(SpE_Cantidad.Text),
                    sMedida = Tb_Medida.Text,
                    dPrecioUnitario =Convert.ToDouble(Tb_PrecioUnitario.Text),
                    dImporte = dImporta

                };
                ReglasClientes.OperacionesVentas.AgregarRenglon(detalle);

                DtG_Ventas.Items.Add(productos);
                Imptotal();

               
                



            }
            catch (Exception ex)
            {
               // MessageBox.Show(ex.ToString());
            }
        }

        private void Tb_IDProducto_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                BusquedaProducto producto = new BusquedaProducto();
                producto.ShowDialog();
            }
        }

        private void CB_Productos_LostFocus(object sender, RoutedEventArgs e)
        {
            ReglasClientes.DatosInventario DI = new ReglasClientes.DatosInventario
            {
                sIDProducto = Tb_IDProducto.Text               
                
            };


            ReglasClientes.DatosProductos datosProductos = new ReglasClientes.DatosProductos()
            {
                sDescripcion = CB_Productos.Text,
                
            };
            ReglasClientes.OperacionesProductos operacionesProductos = new ReglasClientes.OperacionesProductos();
            if (operacionesProductos.ConsultarCB1(ref datosProductos, Tb_IDProducto.Text, ref bExiste))
            {
                Tb_IDProducto.Text = datosProductos.sIDProducto;
                if (CmB_Cliente.Text == "Mayoreo")
                {
                    Tb_PrecioUnitario.Text = Convert.ToString(Math.Round( Convert.ToDouble(datosProductos.dPrecioMayoreo)/1.16,2));
                }
                else if (CmB_Cliente.Text == "Frecuente")
                {
                    Tb_PrecioUnitario.Text = Convert.ToString(Math.Round(Convert.ToDouble(datosProductos.dPrecioFrecuente)/1.16,2));
                }
                else
                {

                    Tb_PrecioUnitario.Text = Convert.ToString(Math.Round(Convert.ToDouble(datosProductos.dPrecioPublico)/1.16,2));
                }
                Tb_Medida.Text = datosProductos.sUnidad;
            }
         
            ReglasClientes.DatosInventario datosInventario = new ReglasClientes.DatosInventario
            { sIDProducto = Tb_IDProducto.Text };
            ReglasClientes.OperacionesInventario inventario = new ReglasClientes.OperacionesInventario();

            Double dCantidad = 0;
            foreach (Listas pa in DtG_Ventas.Items)
            {
                if (pa.Descripcion == CB_Productos.Text)
                {
                    dCantidad = Convert.ToDouble(pa.Cantidad);
                }
            }
            SpE_Cantidad.MaxValue = SpE_Cantidad.MaxValue - Convert.ToDecimal(dCantidad);

            ReglasClientes.OperacionesInventario operacionesInventario = new ReglasClientes.OperacionesInventario();
            if (operacionesInventario.ConsultarStock(DI))
            {
                SpE_Cantidad.MaxValue = Convert.ToDecimal(DI.dStock);
            };


        }

        private void Tb_IDUsuario_LostFocus(object sender, RoutedEventArgs e)
        {
            ReglasClientes.DatosUsuarios datosUsuarios = new ReglasClientes.DatosUsuarios
            {
                sIDUsuario = Tb_IDUsuario.Text
            };
            ReglasClientes.OperacionesUsuarios operacionesUsuarios = new ReglasClientes.OperacionesUsuarios();
            if (operacionesUsuarios.ConsultarUsuario(ref datosUsuarios, Tb_IDUsuario.Text, ref bExiste))
            {
                Tb_NombreUser.Text = datosUsuarios.sNombre;
                Tb_TipoUser.Text = datosUsuarios.sTipoUsuario;
            }
        }

        private void DtG_Ventas_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                switch (sender)
                {
                    case DataGrid grid when grid.Items.Count > 0:
                        int i = grid.SelectedIndex;
                        if (i >= 0 && i < ReglasClientes.OperacionesVentas.detalleVenta.Count)
                        {
                            ReglasClientes.OperacionesVentas.detalleVenta.RemoveAt(i);
                            DtG_Ventas.Items.RemoveAt(i);
                            DtG_Ventas.Items.Refresh();
                            Imptotal();
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ADVERTENCIA", ex.Message);
                // Advertencia("ADVERTENCIA", ex.Message);
            }
        }

        private void Tb_PrecioUnitario_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key >= Key.D0 && e.Key <= Key.D9 || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9 || e.Key == Key.OemPeriod)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void Tb_Descuento_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key >= Key.D0 && e.Key <= Key.D9 || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9 || e.Key == Key.OemPeriod)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void Tb_IDUsuario_Loaded(object sender, RoutedEventArgs e)
        {
            Login login = new Login();
            Tb_IDUsuario.Text = login.Tb_Usuario.Text;
        }

        private void Tb_IDUsuario_Copy_Loaded(object sender, RoutedEventArgs e)
        {
            ReglasClientes.DatosUsuarios datosUsuarios = new ReglasClientes.DatosUsuarios
            {
                sIDUsuario = Tb_IDUsuario_Copy.Text
            };
            ReglasClientes.OperacionesUsuarios operacionesUsuarios = new ReglasClientes.OperacionesUsuarios();
            if (operacionesUsuarios.ConsultarUsuario(ref datosUsuarios, Tb_IDUsuario.Text, ref bExiste))
            {
                Tb_NombreUser_Copy.Text = datosUsuarios.sNombre;
                Tb_TipoUser_Copy.Text = datosUsuarios.sTipoUsuario;
            }
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Tb_IDUsuario_Copy_Loaded_1(object sender, RoutedEventArgs e)
        {
            ReglasClientes.DatosUsuarios datosUsuarios = new ReglasClientes.DatosUsuarios
            {
                sIDUsuario = Tb_IDUsuario_Copy.Text
            };
            ReglasClientes.OperacionesUsuarios operacionesUsuarios = new ReglasClientes.OperacionesUsuarios();
            if (operacionesUsuarios.ConsultarUsuario(ref datosUsuarios, Tb_IDUsuario_Copy.Text, ref bExiste))
            {
                Tb_NombreUser_Copy.Text = datosUsuarios.sNombre;
                Tb_TipoUser_Copy.Text = datosUsuarios.sTipoUsuario;
            }
            bExiste = false;
        }

        private void CB_Productos_GotFocus(object sender, RoutedEventArgs e)
        {
            //if (CmB_Cliente.SelectedIndex == -1)
            //{

            //    MessageBoxResult result = MessageBox.Show("Seleccione un tipo de precio", "Advertencia", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    switch (result)
            //    {
            //        case MessageBoxResult.OK:
            //            CmB_Cliente.Focus();
            //            break;
            //    }
            //}

        }



        private void Tb_IDProducto_LostFocus_1(object sender, RoutedEventArgs e)
        {
            ReglasClientes.DatosProductos datosProductos = new ReglasClientes.DatosProductos()
            {
               // sIDProducto = Tb_IDProducto.Text,
                sCodigoBarras=Tb_IDProducto.Text,
            };
            ReglasClientes.OperacionesProductos operacionesProductos = new ReglasClientes.OperacionesProductos();
            if (operacionesProductos.Consultar1(ref datosProductos, Tb_IDProducto.Text, ref bExiste))
            {
                CB_Productos.Text = datosProductos.sDescripcion;
                Tb_IDProducto.Text = datosProductos.sCodigoBarras;
                if (CmB_Cliente.Text == "Mayoreo")
                {
                    Tb_PrecioUnitario.Text = Convert.ToString(Math.Round(Convert.ToDouble(datosProductos.dPrecioMayoreo)/1.16,2));
                }
                else if (CmB_Cliente.Text == "Frecuente")
                {
                    Tb_PrecioUnitario.Text = Convert.ToString(Math.Round(Convert.ToDouble(datosProductos.dPrecioFrecuente)/1.16,2));
                }
                else
                {
                    Tb_PrecioUnitario.Text = Convert.ToString(Math.Round(Convert.ToDouble(datosProductos.dPrecioPublico)/1.16,2));
                }

                Tb_Medida.Text = datosProductos.sUnidad;

                ReglasClientes.DatosInventario DI = new ReglasClientes.DatosInventario
                {
                    sIDProducto = Tb_IDProducto.Text
                };

                ReglasClientes.OperacionesInventario operacionesInventario = new ReglasClientes.OperacionesInventario();
                if (operacionesInventario.ConsultarStock(DI))
                {
                    SpE_Cantidad.MaxValue = Convert.ToDecimal(DI.dStock);
                };
            }
        }

        private void CB_Productos_KeyUp(object sender, KeyEventArgs e)
        {
            if (CB_Productos.Text.Length == 3)
            {
                CargarProducto();
            }
        }
    }
}
