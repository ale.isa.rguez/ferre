﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;

namespace SistemaDeVenta
{
    /// <summary>
    /// Lógica de interacción para Inventario.xaml
    /// </summary>
    public partial class Inventario : Window
    {
        Boolean bExiste = false;
        public Inventario()
        {
            InitializeComponent();
          
            CB_Producto.SelectedIndex = -1;
            
        }
        private void CargarProducto()
        {
            SqlConnection conexion = new SqlConnection();
            conexion.ConnectionString = ReglasClientes.OperacionesProductos.SCadenaConexion;
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter("SELECT Descripcion FROM Productos  " +
                            $"WHERE (Descripcion) LIKE '%{CB_Producto.Text}%' ", conexion);
            da.Fill(ds, "Productos");
            CB_Producto.ItemsSource = (ds.Tables["Productos"].DefaultView);
            CB_Producto.DisplayMemberPath = ds.Tables["Productos"].Columns["Descripcion"].ToString();


        }
        private void CB_Productos_KeyUp(object sender, KeyEventArgs e)
        {
            if (CB_Producto.Text.Length == 3)
            {
                CargarProducto();
            }
        }
        private void Limpiar()
        {
            CB_Producto.SelectedIndex = -1;
            Tb_Stock.Clear();
            Tb_Medida.Clear();
            Tb_IDproducto.Clear();
            Tb_IDproducto.Focus();
            bExiste = false;

        }

        private void Img_Limpiar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Limpiar();
        }

       
       

        private void CB_Producto_LostFocus(object sender, RoutedEventArgs e)
        {
            ReglasClientes.DatosInventario datosInventario = new ReglasClientes.DatosInventario()
            {

                sDescripcion = CB_Producto.Text
            };
            ReglasClientes.OperacionesInventario operacionesInventario = new ReglasClientes.OperacionesInventario();
            if (operacionesInventario.ConsultarInventario(ref datosInventario))
            {
                Tb_IDproducto.Text = datosInventario.sIDProducto;
                Tb_Stock.Text = Convert.ToString(datosInventario.dStock);
                Tb_Medida.Text = datosInventario.SMedida;
            }
            else
            {
                MessageBox.Show(operacionesInventario.sLastError);
            }

        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close ();
        }

        private void Tb_IDproducto_LostFocus(object sender, RoutedEventArgs e)
        {
            ReglasClientes.DatosInventario datosInventario = new ReglasClientes.DatosInventario()
            {

                sIDProducto=Tb_IDproducto.Text
            };
            ReglasClientes.OperacionesInventario operacionesInventario = new ReglasClientes.OperacionesInventario();
            if (operacionesInventario.ConsultarInventarioID(ref datosInventario))
            {
                CB_Producto.Text = datosInventario.sDescripcion;
                Tb_Stock.Text = Convert.ToString(datosInventario.dStock);
                Tb_Medida.Text = datosInventario.SMedida;
            }
            else
            {
                MessageBox.Show(operacionesInventario.sLastError);
            }
        }
    }
}
