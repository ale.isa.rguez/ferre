namespace SistemaDeVenta
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;
    using System.Drawing.Printing;
    using System.IO;
    using System.Diagnostics;
    using System.Linq;



    /// <summary>
    /// Summary description for Report1.
    /// </summary>
    public partial class Report1 : Telerik.Reporting.Report
    {
        private String sIDVenta;
        Boolean bExiste = false;
        public Report1(String sIDVenta)
        {

            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            this.sIDVenta = sIDVenta;
           
        }

        private void Report1_ItemDataBinding(object sender, EventArgs e)
        {

            ReglasClientes.OperacionesProductos operacionesProductos = new ReglasClientes.OperacionesProductos();
            this.DataSource = operacionesProductos.ConsultarVentasT(sIDVenta);

        }

     
        public void PrintReport(object sender, EventArgs e)
        {
            
            using (var p = new Process())
            {
                
                p.StartInfo.FileName = @"C:\SistemaDeVenta\bin\Debug\Ticket.PDF";
                p.StartInfo.Verb = "Print";

                p.Start();
                //libreria Threading
                System.Threading.Thread.Sleep(3000);
                p.CloseMainWindow();
               
               
               
            }
          
        
        }

      
        public void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Report1 report = new Report1(sIDVenta);
            string pdfPath = Path.Combine(Application.StartupPath, "Ticket.pdf");

            System.Diagnostics.Process.Start(pdfPath);          


            e.Graphics.DrawString(pdfPath, new System.Drawing.Font("Arial", 8), Brushes.Black, 10, 25);
           
        }
    }
}