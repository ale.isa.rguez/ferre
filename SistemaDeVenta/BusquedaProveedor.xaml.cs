﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;

namespace SistemaDeVenta
{
    /// <summary>
    /// Lógica de interacción para BusquedaProveedor.xaml
    /// </summary>
    public partial class BusquedaProveedor : Window
    {
        public BusquedaProveedor()
        {
            InitializeComponent();


        }
        private String SCadenaConexion = ReglasClientes.OperacionesProductos.SCadenaConexion;
        public void Buscar()
        {
            using (SqlConnection conn = new SqlConnection(SCadenaConexion))
            {
                conn.Open();
                try
                {

                    string sQry = "SELECT RFC,Nombre,Telefono,CorreoElectronico " +
                        "FROM Proveedores " +
                        $"WHERE (Nombre) LIKE '%{Tb_BuscarProv.Text}%' ";
                    SqlCommand comando = new SqlCommand(sQry, conn);

                    DataTable tabla = new DataTable();
                    SqlDataAdapter adapter = new SqlDataAdapter(comando);
                    adapter.Fill(tabla);
                    DataG_BuscarPov.ItemsSource = tabla.DefaultView;
                    conn.Close();
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());

                }
            }
        }
        private void Tb_BuscarProv_KeyUp(object sender, KeyEventArgs s)
        {
            if (s.Key == Key.Enter)
            {
                Buscar();
            }
            if (s.Key == Key.Escape)
            {
                ID();
                this.Close();

            }
        }

        private void DataG_BuscarPov_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ComprasProducto compras = new ComprasProducto();
            // compras.Tb_ID.Text = Convert.ToString(DataG_BuscarPov.SelectedCells[0].Column.GetCellContent(0));
            //compras.Tb_IDProv.Text = Convert.ToString(((DataRowView)DataG_BuscarPov.Items[DataG_BuscarPov.SelectedIndex])[0]);
        }
        public void ID()
            {
            ComprasProducto compras = new ComprasProducto();
            String ID;

          // ID = Convert.ToString(DataG_BuscarPov.SelectedCells[1].Column.GetCellContent(1));
            //compras.Tb_IDProv.Text = Convert.ToString(((DataRowView)DataG_BuscarPov.Items[DataG_BuscarPov.SelectedIndex])[0]);
        }
       
        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                
                this.Close();

            }
        }

        

        private void DataG_BuscarPov_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {  
          
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}
