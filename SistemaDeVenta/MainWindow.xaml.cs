﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SistemaDeVenta
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
          
            
        }

        public void validar()
        {
           
            if (Tb_Usuario.Text == "Vendedor")
            {
                Img_Compras.Visibility =Visibility.Collapsed;
                Img_Productos.Visibility = Visibility.Hidden;
                Img_Clientes.Visibility = Visibility.Hidden;
                
            }
        }

        private void Image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Proveedores proveedores = new Proveedores();
            proveedores.ShowDialog();
            //Login login = new Login();
            //login.ShowDialog();
        }

        private void Img_Ventas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Ventas ventas = new Ventas();
            ventas.Tb_IDUsuario_Copy.Text = Tb_Usuario_ID.Text;
            ventas.ShowDialog();

        }

        private void Img_Inventario_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Inventario inventario = new Inventario();
            inventario.ShowDialog();
        }

        private void Img_Productos_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Productos productos = new Productos();
            productos.ShowDialog();
        }

        private void Img_Clientes_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Clientes clientes = new Clientes();
            clientes.ShowDialog();
        }

        private void Img_Compras_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ComprasProducto comprasProducto = new ComprasProducto();
            comprasProducto.ShowDialog();
        }

        
        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            validar();
        }

        private void Tb_Usuario_ID_Loaded(object sender, RoutedEventArgs e)
        {
           

        }

        private void Img_Presupuesto_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Presupuestos presupuestos = new Presupuestos();
            presupuestos.Tb_IDUsuario_Copy.Text = Tb_Usuario_ID.Text;
            presupuestos.ShowDialog();
        }

        private void Img_Reportes_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ReportesV reportes = new ReportesV();
            reportes.ShowDialog();
        }
    }
}

