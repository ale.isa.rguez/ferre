﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
namespace SistemaDeVenta
{
    /// <summary>
    /// Lógica de interacción para Productos.xaml
    /// </summary>
    public partial class Productos : Window
    {
        Boolean bExiste = false;
        public Productos()
        {
            InitializeComponent();
            Tb_IDproducto.Focus();
            CargarProveedor();
            Img_Eliminar.Visibility = Visibility.Hidden;
        }
        private void CargarProveedor()
        {

            SqlConnection conexion = new SqlConnection();

            conexion.ConnectionString = ReglasClientes.OperacionesProductos.SCadenaConexion;
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter("SELECT Nombre FROM Proveedores", conexion);
            da.Fill(ds, "Proveedores");
            CmB_Proveedor.ItemsSource = (ds.Tables["Proveedores"].DefaultView);
            CmB_Proveedor.DisplayMemberPath = ds.Tables["Proveedores"].Columns["Nombre"].ToString();
        }

        private void Limpiar()
        {
            Tb_IDproducto.Clear();
            CmB_Proveedor.SelectedIndex = -1;
            CmB_Unidad.SelectedIndex = -1;
            Tb_Nombre.Clear();
            Tb_PrecioFrecuente.Clear();
            Tb_PrecioMayoreo.Clear();
            Tb_PrecioPublico.Clear();
            Tb_Minimo.Clear();
            Tb_CodigoB.Clear();
            Tb_IDproducto.Focus();
            bExiste = false;
        }
        private void Image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (CmB_Proveedor.SelectedIndex == -1 || Tb_IDproducto.Text.Length == 0 || Tb_Nombre.Text.Length == 0 || Tb_PrecioMayoreo.Text.Length == 0 || Tb_PrecioFrecuente.Text.Length == 0 || Tb_PrecioPublico.Text.Length == 0 || Tb_Minimo.Text.Length == 0 || CmB_Unidad.SelectedIndex == -1)
                {
                    MessageBox.Show("Verifique que no existan  campos vacios","Advertencia", MessageBoxButton.OK,  MessageBoxImage.Exclamation);
                }
                else
                {

                    ReglasClientes.DatosProductos datosProductos = new ReglasClientes.DatosProductos()
                    {
                        sIDProducto = Tb_IDproducto.Text,                       
                        sDescripcion = Tb_Nombre.Text,
                        dPrecioMayoreo = Convert.ToDouble(Tb_PrecioMayoreo.Text),
                        dPrecioFrecuente =Convert.ToDouble(Tb_PrecioFrecuente.Text),
                        dPrecioPublico = Convert.ToDouble(Tb_PrecioPublico.Text),
                        dMinimo = Convert.ToDouble(Tb_Minimo.Text),
                        sUnidad = CmB_Unidad.Text,
                        sCodigoBarras = Tb_CodigoB.Text

                    };
                    ReglasClientes.DatosProveedor datosProveedor = new ReglasClientes.DatosProveedor()
                    {
                        sRFC = CmB_Proveedor.Text,
                        sNombre = CmB_Proveedor.Text

                    };
                    ReglasClientes.DatosInventario datosInventario = new ReglasClientes.DatosInventario
                    {
                        dStock = 0,
                        sDescripcion = Tb_Nombre.Text,
                        SMedida = CmB_Unidad.Text
                    };
                    if (!bExiste)
                    {


                        ReglasClientes.OperacionesProductos OperacionProducto = new ReglasClientes.OperacionesProductos();
                        {
                            if (!OperacionProducto.AltaPruducto(datosProductos, datosProveedor, datosInventario))
                            {
                                MessageBox.Show(OperacionProducto.sLastError);
                            }
                        }
                        bExiste = true;
                    }
                    else if (bExiste)
                    {

                        ReglasClientes.OperacionesProductos OperacionProducto = new ReglasClientes.OperacionesProductos();
                        {
                            if (!OperacionProducto.Actualizar(datosProductos))
                            {
                                MessageBox.Show(OperacionProducto.sLastError);
                            }

                            if (OperacionProducto.ProdProv(datosProductos, datosProveedor))
                            {

                            }
                            else
                            {
                                MessageBox.Show("Ya existe una relación del producto con este proveedor");
                            }




                        }
                    }
                    Limpiar();
                }
            }
            catch
            {

            }
            
        }

        private void Img_Limpiar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Limpiar();
        }

        private void Img_Eliminar_MouseDown(object sender, MouseButtonEventArgs e)
        {

            MessageBox.Show("¿Seguro que  desea eliminar este producto?", MessageBoxButton.YesNo.ToString());
            

            ReglasClientes.OperacionesProductos operacionesProductos = new ReglasClientes.OperacionesProductos();
            if (!operacionesProductos.Eliminar(Tb_IDproducto.Text))
            {
                MessageBox.Show(operacionesProductos.sLastError);
            }
            Limpiar();
        }

        private void Tb_IDproducto_LostFocus(object sender, RoutedEventArgs e)
        {
            ReglasClientes.DatosProductos datosProductos = new ReglasClientes.DatosProductos
            {
                sIDProducto = Tb_IDproducto.Text
            };
            ReglasClientes.DatosProveedor datosProveedor = new ReglasClientes.DatosProveedor
            {
                sNombre= CmB_Proveedor.Text
                
            };
            ReglasClientes.OperacionesProductos operacionesProductos = new ReglasClientes.OperacionesProductos();    
            if (operacionesProductos.Consultar(ref datosProductos,Tb_IDproducto.Text,ref bExiste))
            {
                Tb_Nombre.Text = datosProductos.sDescripcion;               
                Tb_PrecioMayoreo.Text =Convert.ToString( datosProductos.dPrecioMayoreo);
                Tb_PrecioFrecuente.Text = Convert.ToString(datosProductos.dPrecioFrecuente);
                Tb_PrecioPublico.Text = Convert.ToString(datosProductos.dPrecioPublico);                
                Tb_Minimo.Text = Convert.ToString(datosProductos.dMinimo);
                CmB_Unidad.Text = datosProductos.sUnidad;
                Tb_CodigoB.Text = datosProductos.sCodigoBarras;

            }
           
            else
            {
                MessageBox.Show(operacionesProductos.sLastError);
            }
        }

        private void Tb_IDproducto_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                try
                { 
                BusquedaProducto busquedaProducto = new BusquedaProducto();
                busquedaProducto.ShowDialog();
                Tb_IDproducto.Text = Convert.ToString(((DataRowView)busquedaProducto.DataG_BuscarCliente.Items[busquedaProducto.DataG_BuscarCliente.SelectedIndex])[0]);
                }
                catch
                {
                    MessageBox.Show("No se seleccionó ningún resultado", "Advertencia", MessageBoxButton.OK, MessageBoxImage.Warning);
                }

            }
        }

        private void Tb_IDproducto_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key >= Key.D0 && e.Key <= Key.D9 || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }   

        private void Tb_PrecioVenta_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key >= Key.D0 && e.Key <= Key.D9 || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9 || e.Key == Key.OemPeriod)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Tb_PrecioMayoreo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key >= Key.D0 && e.Key <= Key.D9 || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9 || e.Key == Key.OemPeriod)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}