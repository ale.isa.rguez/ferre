﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;

namespace SistemaDeVenta
{
    /// <summary>
    /// Lógica de interacción para Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
            Tb_Usuario.Focus();
        }

        private void Tb_Entrar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Tb_Usuario.Text.Length == 0 || Tb_Password.Password.Length == 0 || CB_TipoUser.Text.Length == 0)
                {
                    MessageBox.Show("Verifique que no existan  campos vacios", "Advertencia", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    DataTable dtLogin = new DataTable();
                    dtLogin.Clear();

                    String Consulta = $"Select * FROM Usuarios WHERE IDUsuario ='{Tb_Usuario.Text}'";

                    dtLogin = ReglasClientes.OperacionesLogin.ConsultaTablas(Consulta);
                    ReglasClientes.DatosLogin.sIDUsuario = dtLogin.Rows[0][0].ToString();
                    ReglasClientes.DatosLogin.sNombre = dtLogin.Rows[0][1].ToString();
                    ReglasClientes.DatosLogin.sPassword = dtLogin.Rows[0][2].ToString();
                    ReglasClientes.DatosLogin.sTipoUsuario = dtLogin.Rows[0][3].ToString();


                    if (!string.IsNullOrEmpty(Tb_Usuario.Text) && !string.IsNullOrEmpty(Tb_Password.Password))
                    {
                        if (Tb_Usuario.Text == ReglasClientes.DatosLogin.sIDUsuario && Tb_Password.Password == ReglasClientes.DatosLogin.sPassword && CB_TipoUser.Text == ReglasClientes.DatosLogin.sTipoUsuario)
                        {
                            MainWindow menu = new MainWindow();
                            Ventas ventas = new Ventas();
                            Presupuestos presupuestos = new Presupuestos();
                            menu.Tb_Usuario.Text = CB_TipoUser.Text;
                            ventas.Tb_IDUsuario.Text = Tb_Usuario.Text;
                            presupuestos.Tb_IDUsuario.Text = Tb_Usuario.Text;


                            menu.Tb_Usuario_ID.Text = Tb_Usuario.Text;
                            menu.Tb_Usuario.Text = CB_TipoUser.Text;
                            menu.ShowDialog();


                        }
                        else
                        {

                            MessageBox.Show("Verifique sus datos", "Advertencia", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        }


                    }
                }
            }
            catch
            {
                //   FuncionesGenerales.Mensajes.ToMessageError(tbUsuario, "El usuario no existe");
                MessageBox.Show("El usuario no existe", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Tb_Usuario_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.Key >= Key.D0 && e.Key <= Key.D9 || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9)
            //{
            //    e.Handled = false;
            //}
            //else
            //{
            //    e.Handled = true;
            //}
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
