﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SistemaDeVenta
{
    /// <summary>
    /// Lógica de interacción para Clientes.xaml
    /// </summary>
    public partial class Clientes : Window
    {
        Boolean bExiste = false;
        public Clientes()
        {
            InitializeComponent();
            Tb_ID.Focus();
            Img_Eliminar.Visibility= Visibility.Hidden;
        }
        private void Limpiar()
        {
            Tb_ID.Clear();            
            Tb_Nombre.Clear();
            Tb_Password.Clear();
            
            Tb_ID.Focus();
            bExiste = false;
        }

        private void Img_Guardar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Tb_ID.Text.Length == 0 || Tb_Nombre.Text.Length == 0 || Tb_Password.Password.Length == 0 || CB_TipoU.SelectedIndex == -1)
            {
                MessageBox.Show("Verifique que no existan  campos vacios", "Advertencia", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else
            {
                ReglasClientes.DatosUsuarios datosUsuarios = new ReglasClientes.DatosUsuarios()
                {
                    sIDUsuario = Tb_ID.Text,
                    sNombre = Tb_Nombre.Text,
                    sPassword = Tb_Password.Password,
                    sTipoUsuario = CB_TipoU.Text

                };
                if (!bExiste)
                {

                    ReglasClientes.OperacionesUsuarios operacionesUsuarios = new ReglasClientes.OperacionesUsuarios();
                    {
                        if (!operacionesUsuarios.AltaUsuario(datosUsuarios))
                        {
                            MessageBox.Show(operacionesUsuarios.sLastError);
                        }

                    }
                    bExiste = true;
                }
                else
                {
                    ReglasClientes.OperacionesUsuarios operacionesUsuarios = new ReglasClientes.OperacionesUsuarios();
                    {
                        if (!operacionesUsuarios.ActualizarUsuario(datosUsuarios))
                        {
                            MessageBox.Show(operacionesUsuarios.sLastError);
                        }

                    }
                }
                Limpiar();
            }
        }

        private void Img_Eliminar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ReglasClientes.DatosUsuarios datosUsuarios = new ReglasClientes.DatosUsuarios()
            {
                sIDUsuario = Tb_ID.Text,
                sNombre = Tb_Nombre.Text,
                sPassword=Tb_Password.Password,
                sTipoUsuario=CB_TipoU.Text
               
            };
            ReglasClientes.OperacionesUsuarios operacionesUsuarios = new ReglasClientes.OperacionesUsuarios();
            {
                if (!operacionesUsuarios.EliminarUsuario(datosUsuarios))
                {
                    MessageBox.Show(operacionesUsuarios.sLastError);
                }

            }
            Limpiar();
        }

        private void Im_Limpiar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Limpiar();
        }

        private void Tb_ID_LostFocus(object sender, RoutedEventArgs e)
        {
            ReglasClientes.DatosUsuarios datosUsuarios = new ReglasClientes.DatosUsuarios()
            {
                sIDUsuario = Tb_ID.Text
            
             };
            ReglasClientes.OperacionesUsuarios operacionesUsuarios = new ReglasClientes.OperacionesUsuarios();
            {
                
                if (operacionesUsuarios.ConsultarUsuario(ref datosUsuarios, Tb_ID.Text,ref bExiste))
                {
                    
                    Tb_Nombre.Text = datosUsuarios.sNombre;
                    Tb_Password.Password = datosUsuarios.sPassword;
                    CB_TipoU.Text = datosUsuarios.sTipoUsuario;
                   
                }
                else
                {
                    MessageBox.Show(operacionesUsuarios.sLastError);
                }

            }
        }
       
        private void Tb_ID_KeyDown(object sender, KeyEventArgs e)
        {
            Window1 Busqueda = new Window1();
            if (e.Key == Key.F1)
            {
                try {
                    Busqueda.ShowDialog();
                    Tb_ID.Text = Convert.ToString(((DataRowView)Busqueda.DataG_BuscarCliente.Items[Busqueda.DataG_BuscarCliente.SelectedIndex])[0]);
                }
                catch
                {
                    MessageBox.Show("No se seleccionó ningún resultado", "Advertencia", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

      
    }
}
