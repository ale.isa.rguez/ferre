﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace ReglasClientes
{
    public class OperacionesInventario
    {
        public Boolean bExiste = false;
        public String sLastError = "";
        public String sServidor;
        public String sBaseDatos;
        public String sUsuario;
        public String sPassword;

        public OperacionesInventario()
        {
            this.sServidor = sServidor;
            this.sBaseDatos = sBaseDatos;
            this.sUsuario = sUsuario;
            this.sPassword = sPassword;
        }
        public Boolean AltaInventario(DatosInventario dato)
        {
            Boolean bAllok = false;
            if (!bExiste)
            {
                using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                    try
                    {
                        conn.Open();
                        SqlCommand comando = new SqlCommand();
                        {
                            comando.Connection = conn;
                            comando.CommandText = "INSERT INTO Inventario (IDProducto,Stock)VALUES (@IDProducto,@Stock)";
                        };
                        comando.Parameters.Add("@IDProducto", SqlDbType.VarChar, 10);
                        comando.Parameters.Add("Stock", SqlDbType.Float);


                        comando.Parameters["@IDProducto"].Value = dato.sIDProducto;
                        comando.Parameters["Stock"].Value = dato.dStock;


                        comando.ExecuteNonQuery();
                        conn.Close();
                        bAllok = true;
                        bExiste = true;
                    }
                    catch (Exception ex)
                    {
                        sLastError = ex.Message;
                    }
            }
            else
            {
                using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                    try
                    {
                        conn.Open();
                        SqlCommand comando = new SqlCommand();
                        {
                            comando.Connection = conn;
                            comando.CommandText = " UPDATE Inventario SET Stock=Stock+@Stock WHERE IDProducto=@IDProducto";
                        };
                        comando.Parameters.Add("IDProducto", SqlDbType.VarChar, 10);
                        comando.Parameters.Add("Stock", SqlDbType.Float, 10);


                        comando.Parameters["iDProducto"].Value = dato.sIDProducto;
                        comando.Parameters["Stock"].Value = dato.dStock;


                        comando.ExecuteNonQuery();
                        conn.Close();
                        bAllok = true;
                        bExiste = true;
                    }
                    catch (Exception ex)
                    {
                        sLastError = ex.Message;
                    }

            }

            
            return bAllok;

        }
        public Boolean ActuarInventario(DatosInventario dato)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comando = new SqlCommand();
                    {
                        comando.Connection = conn;
                        comando.CommandText = "UPDATE Inventario SET Stock=@Stock WHERE IDProducto=@IDProducto";
                    };
                    comando.Parameters.Add("@IDProducto", SqlDbType.VarChar, 10);
                    comando.Parameters.Add("@Stock", SqlDbType.Float);
                   

                    comando.Parameters["@IDProducto"].Value = dato.sIDProducto;
                    comando.Parameters["@Stock"].Value = dato.dStock;
                    


                    comando.ExecuteNonQuery();
                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;

        }
        public Boolean EliminarInventario(DatosInventario dato, DatosProductos datosProductos)
        {
            Boolean bAllok = false;
           
            return bAllok;

        }
        public Boolean ConsultarInventario(ref DatosInventario dato )
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comando = new SqlCommand();
                    {
                        comando.Connection = conn;
                        comando.CommandText = "SELECT IDProducto,Stock,Medida FROM Inventario WHERE Descripcion=@Descripcion ";
                    };
                   
                  
                    comando.Parameters.Add("@Descripcion", SqlDbType.VarChar, 100);
                    comando.Parameters["@Descripcion"].Value = dato.sDescripcion;


                    SqlDataReader reader = comando.ExecuteReader();
                    //dato.sIDProducto = sID;
                    while (reader.Read())
                    {
                        dato.sIDProducto = reader[0].ToString();
                        dato.dStock = Convert.ToDouble(reader[1].ToString());
                        dato.SMedida = reader[2].ToString();
                      
                    }

                   

                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;

        }
        public Boolean ConsultarInventarioID(ref DatosInventario dato)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comando = new SqlCommand();
                    {
                        comando.Connection = conn;
                        comando.CommandText = "SELECT Descripcion,Stock,Medida FROM Inventario WHERE IDProducto=(Select IDProducto FROM Productos Where CodigoBarras=@CodigoBarras) ";
                    };


                    comando.Parameters.Add("@CodigoBarras", SqlDbType.VarChar, 15);
                    comando.Parameters["@CodigoBarras"].Value = dato.sIDProducto;


                    SqlDataReader reader = comando.ExecuteReader();
                    //dato.sIDProducto = sID;
                    while (reader.Read())
                    {
                        dato.sDescripcion= reader[0].ToString();
                        dato.dStock = Convert.ToDouble(reader[1].ToString());
                        dato.SMedida = reader[2].ToString();

                    }



                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;

        }
        public Boolean ConsultarProductos( DatosProductos datos )
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comando = new SqlCommand();
                    {
                        comando.Connection = conn;
                        comando.CommandText = "SELECT descripcion FROM Productos ";
                    };


                  //  comando.Parameters.Add("@IDProducto", SqlDbType.VarChar, 100);
                    //comando.Parameters["@IDProducto"].Value = dato.sIDProducto;
                   // comando.Parameters.Add("@CodigoBarras", SqlDbType.VarChar, 15);
                   // comando.Parameters["@CodigoBarras"].Value = dato.sIDProducto;


                    SqlDataReader reader = comando.ExecuteReader();
                    //dato.sIDProducto = sID;
                    List<String> productos = new List<String>();
                    while (reader.Read())
                    {
                        // (reader.ToString());
                        productos.Add(reader[0].ToString());
                    }



                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;

        }
        public Boolean ConsultarStock(DatosInventario dato)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comando = new SqlCommand();
                    {
                        comando.Connection = conn;
                        comando.CommandText = "SELECT Stock FROM Inventario WHERE IDProducto=(Select IDProducto FROM Productos Where CodigoBarras=@CodigoBarras) ";
                    };


                    //comando.Parameters.Add("@IDProducto", SqlDbType.VarChar, 100);
                    //comando.Parameters["@IDProducto"].Value = dato.sIDProducto;
                    comando.Parameters.Add("@CodigoBarras", SqlDbType.VarChar, 15);
                    comando.Parameters["@CodigoBarras"].Value = dato.sIDProducto;


                    SqlDataReader reader = comando.ExecuteReader();
                    //dato.sIDProducto = sID;
                    while (reader.Read())
                    {
                        dato.dStock = Convert.ToDouble(reader[0].ToString());


                    }



                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;

        }

    }
}
