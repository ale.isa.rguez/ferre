﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace ReglasClientes
{
   public class OperacionesUsuarios
    {
        
            public String sLastError = "";
            public String sServidor;
            public String sBaseDatos;
            public String sUsuario;
            public String sPassword;

            public OperacionesUsuarios()
            {
                this.sServidor = sServidor;
                this.sBaseDatos = sBaseDatos;
                this.sUsuario = sUsuario;
                this.sPassword = sPassword;
            }
            public Boolean AltaUsuario(DatosUsuarios dato)
            {
                Boolean bAllok = false;
                using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                    try
                    {
                        conn.Open();
                        SqlCommand comando = new SqlCommand();
                        {
                            comando.Connection = conn;
                        comando.CommandText = "INSERT INTO Usuarios (IDUsuario,Nombre,Password,TipoUsuario) VALUES (@IDUsuario,@Nombre,@Password,@TipoUsuario)";
                        };
                        comando.Parameters.Add("@IDUsuario", SqlDbType.VarChar, 10);
                        comando.Parameters.Add("@Nombre", SqlDbType.VarChar, 50);
                        comando.Parameters.Add("@Password", SqlDbType.VarChar, 20);
                        comando.Parameters.Add("@TipoUsuario", SqlDbType.VarChar, 20);
                       

                        comando.Parameters["@IDUsuario"].Value = dato.sIDUsuario;
                        comando.Parameters["@Nombre"].Value = dato.sNombre;
                        comando.Parameters["@Password"].Value = dato.sPassword;
                        comando.Parameters["@TipoUsuario"].Value = dato.sTipoUsuario;
                       

                        comando.ExecuteNonQuery();
                        conn.Close();
                        bAllok = true;
                    }
                    catch (Exception ex)
                    {
                        sLastError = ex.Message;
                    }
                return bAllok;

            }
            public Boolean ActualizarUsuario(DatosUsuarios dato)
            {
                Boolean bAllok = false;
                using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                    try
                    {
                        conn.Open();
                        SqlCommand comando = new SqlCommand();
                        {
                            comando.Connection = conn;
                            comando.CommandText = "UPDATE Usuarios SET Nombre=@Nombre,Password=@Password,TipoUsuario=@TipoUsuario WHERE IDUsuario=@IDUsuario";
                        };
                    comando.Parameters.Add("@IDUsuario", SqlDbType.VarChar, 10);
                    comando.Parameters.Add("@Nombre", SqlDbType.VarChar, 50);
                    comando.Parameters.Add("@Password", SqlDbType.VarChar, 20);
                    comando.Parameters.Add("@TipoUsuario", SqlDbType.VarChar, 20);


                    comando.Parameters["@IDUsuario"].Value = dato.sIDUsuario;
                    comando.Parameters["@Nombre"].Value = dato.sNombre;
                    comando.Parameters["@Password"].Value = dato.sPassword;
                    comando.Parameters["@TipoUsuario"].Value = dato.sTipoUsuario;

                    comando.ExecuteNonQuery();
                        conn.Close();
                        bAllok = true;
                    }
                    catch (Exception ex)
                    {
                        sLastError = ex.Message;
                    }
                return bAllok;

            }
            public Boolean EliminarUsuario(DatosUsuarios dato)
            {
                Boolean bAllok = false;
                using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                    try
                    {
                        conn.Open();
                        SqlCommand comando = new SqlCommand();
                        {
                            comando.Connection = conn;
                            comando.CommandText = "DELETE FROM Usuarios WHERE IDUsuario= @IDUsuario";
                        };
                        comando.Parameters.Add("IDUsuario", SqlDbType.VarChar, 10);                   

                        comando.Parameters["IDUsuario"].Value = dato.sIDUsuario;
                       

                        comando.ExecuteNonQuery();
                        conn.Close();
                        bAllok = true;
                    }
                    catch (Exception ex)
                    {
                        sLastError = ex.Message;
                    }
                return bAllok;

            }
            public Boolean ConsultarUsuario(ref DatosUsuarios dato, String sID, ref Boolean bExite)
            {
                Boolean bAllok = false;
                using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                    try
                    {
                        conn.Open();
                        SqlCommand comando = new SqlCommand();
                        {
                            comando.Connection = conn;
                            comando.CommandText = "SELECT Nombre,Password,TipoUsuario FROM Usuarios WHERE IDUsuario=@IDUsuario";
                        };
                        comando.Parameters.Add("IDUsuario", SqlDbType.VarChar, 10);                        
                        comando.Parameters["IDUsuario"].Value = dato.sIDUsuario;

                        SqlDataReader reader = comando.ExecuteReader();
                        dato.sIDUsuario = sID;
                        while (reader.Read())
                        {
                        bExite = true;
                            dato.sNombre = reader[0].ToString();
                            dato.sPassword = reader[1].ToString();                           
                            dato.sTipoUsuario = reader[2].ToString();
                        }

                       // comando.ExecuteNonQuery();
                        conn.Close();
                        bAllok = true;
                    }
                    catch (Exception ex)
                    {
                        sLastError = ex.Message;
                    }
                return bAllok;

            }



        }
    }

