﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Windows;


namespace ReglasClientes
{
    public class OperacionesVentas
    {
        //public Boolean bExiste = false;
        public String sLastError = "";
        public String sServidor;
        public String sBaseDatos;
        public String sUsuario;
        public String sPassword;
        public String Stock;
        public String Minimo;
        //Variables para ConsultaProductos
        static SqlConnection cnn = new SqlConnection(OperacionesProductos.SCadenaConexion);
        static SqlCommand comando;
        static DataSet ds;
        static DataTable data;
        static DataTable DtVacio = new DataTable();

        public OperacionesVentas()
        {
            this.sServidor = sServidor;
            this.sBaseDatos = sBaseDatos;
            this.sUsuario = sUsuario;
            this.sPassword = sPassword;
            //detalleVenta.Clear();
        }
        public static ArrayList detalleVenta = new ArrayList();

        #region Ventas
        public Boolean AltaVentas(DatosVentas dato, DatosVentasDetalle detalle, DatosInventario datosInventario, DatosProductos datosProduc)
        {
            Boolean bAllok = false;

            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
            {
                SqlTransaction transaction = null;
                try
                {
                    conn.Open();
                    transaction = conn.BeginTransaction();

                    SqlCommand comando = new SqlCommand();
                    {
                        comando.Connection = conn;
                        comando.Transaction = transaction;
                        comando.CommandText = "INSERT INTO Ventas (Folio,IDUsuario,Fecha,Descuento,Subtotal,IVA,Total)" +
                            "VALUES (@Folio,@IDUsuario,@Fecha,@Descuento,@Subtotal,@IVA,@Total)";

                    };
                    comando.Parameters.Add("@Folio", SqlDbType.VarChar, 10);
                    comando.Parameters.Add("@IDUsuario", SqlDbType.VarChar, 10);
                    comando.Parameters.Add("@Fecha", SqlDbType.Date);
                    comando.Parameters.Add("@Descuento", SqlDbType.Float);
                    comando.Parameters.Add("@Subtotal", SqlDbType.Float);
                    comando.Parameters.Add("@IVA", SqlDbType.Float);
                    comando.Parameters.Add("@Total", SqlDbType.Float);

                    comando.Parameters["@Folio"].Value = dato.sFolio;
                    comando.Parameters["@IDUsuario"].Value = dato.sIDUsuario;
                    comando.Parameters["@Fecha"].Value = dato.Fecha;
                    comando.Parameters["@Descuento"].Value = dato.dDescuento;
                    comando.Parameters["@Subtotal"].Value = dato.dSubtotal;
                    comando.Parameters["@IVA"].Value = dato.dIVA;
                    comando.Parameters["@Total"].Value = dato.dTotal;

                    comando.ExecuteNonQuery();

                    foreach (DatosVentasDetalle row in detalleVenta)

                    //
                    //for  (Int32 nIndex = 0; nIndex < detalleVenta.Count; nIndex++)
                    {
                        // DatosVentasDetalle row = GetRenglon(nIndex);
                        SqlCommand sqlCommandDetalle = new SqlCommand();
                        sqlCommandDetalle.Transaction = transaction;
                        sqlCommandDetalle.Connection = conn;
                        sqlCommandDetalle.CommandText = "INSERT INTO VentasDetalle (Folio,IDProducto,Descripcion,Cantidad,Medida,PrecioUnitario,Importe)" +
                          "VALUES (@Folio,(SELECT IDProducto FROM Productos Where Descripcion=@Descripcion),@Descripcion,@Cantidad,@Medida,@PrecioUnitario,@Importe)";


                        sqlCommandDetalle.Parameters.Add("@Folio", SqlDbType.VarChar, 10);
                        sqlCommandDetalle.Parameters.Add("@IDProducto", SqlDbType.VarChar, 10);
                        sqlCommandDetalle.Parameters.Add("@Descripcion", SqlDbType.VarChar, 100);
                        sqlCommandDetalle.Parameters.Add("@Medida", SqlDbType.VarChar, 10);
                        sqlCommandDetalle.Parameters.Add("@Cantidad", SqlDbType.Float);
                        sqlCommandDetalle.Parameters.Add("@PrecioUnitario", SqlDbType.Float);
                        sqlCommandDetalle.Parameters.Add("@Importe", SqlDbType.Float);


                        sqlCommandDetalle.Parameters["@Folio"].Value = row.sFolio;
                        sqlCommandDetalle.Parameters["@IDProducto"].Value = row.sIDProducto;
                        sqlCommandDetalle.Parameters["@Descripcion"].Value = row.sDescripcion;
                        sqlCommandDetalle.Parameters["@Cantidad"].Value = row.dCantidad;
                        sqlCommandDetalle.Parameters["@Medida"].Value = row.sMedida;
                        sqlCommandDetalle.Parameters["@PrecioUnitario"].Value = row.dPrecioUnitario;
                        sqlCommandDetalle.Parameters["@Importe"].Value = row.dImporte;
                        sqlCommandDetalle.ExecuteNonQuery();

                        //DESCONTAR DE INVENTARIO
                        SqlCommand sqlCommandInventario = new SqlCommand();
                        sqlCommandInventario.Connection = conn;
                        sqlCommandInventario.Transaction = transaction;

                        sqlCommandInventario.CommandText = $"UPDATE Inventario " +
                                                        "SET Stock=Stock-@Cantidad " +
                                                        "WHERE IDProducto =(Select IDProducto FROM Productos Where CodigoBarras=@CodigoBarras) ";

                        sqlCommandInventario.Parameters.Add("@CodigoBarras", SqlDbType.VarChar, 20);
                        sqlCommandInventario.Parameters.Add("@Cantidad", SqlDbType.Float);

                        sqlCommandInventario.Parameters["@CodigoBarras"].Value = row.sIDProducto;
                        sqlCommandInventario.Parameters["@Cantidad"].Value = row.dCantidad;
                        sqlCommandInventario.ExecuteNonQuery();



                    }
                    transaction.Commit();
                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                    transaction.Rollback();
                }
            }
            return bAllok;

        }
        public Boolean Notificacion(DatosVentasDetalle dato, ref ArrayList Stock1, ref ArrayList Minimo1, ref ArrayList IDP)
        {
            Boolean bAllok = false;

            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                try
                {
                    conn.Open();

                    SqlCommand comandoF = new SqlCommand();
                    {
                        comandoF.Connection = conn;

                        Stock = comandoF.CommandText = "SELECT IDProducto  FROM VentasDetalle  WHERE Folio=@Folio";

                    };
                    comandoF.Parameters.Add("@Folio", SqlDbType.VarChar, 10);
                    comandoF.Parameters["@Folio"].Value = dato.sFolio;

                    SqlDataReader readerID = comandoF.ExecuteReader();

                    //for (int i = 0; i < Convert.ToInt16(readerID.Read()); i++)
                    //{ 


                    while (readerID.Read())
                    {
                        IDP.Add(readerID[0].ToString());
                    }



                    //}

                    readerID.Close();
                    ////////////////////////////////////////////////
                    for (int x = 0; x < IDP.Count; x++)
                    {


                        SqlCommand comando = new SqlCommand();
                        {
                            comando.Connection = conn;

                            Stock = comando.CommandText = "SELECT Stock FROM Inventario WHERE IDProducto=@IDProducto";

                        };
                        comando.Parameters.Add("@IDProducto", SqlDbType.VarChar, 10);
                        comando.Parameters["@IDProducto"].Value = IDP[x];

                        SqlDataReader reader = comando.ExecuteReader();

                        while (reader.Read())
                        {

                            Stock1.Add(reader[0].ToString());
                        }

                        reader.Close();
                    }
                    for (int x = 0; x < IDP.Count; x++)
                    {
                        ////////////////////////////////////////////////
                        SqlCommand comandoM = new SqlCommand();
                        {
                            comandoM.Connection = conn;
                            comandoM.CommandText = "SELECT Minimo  FROM Productos  WHERE IDProducto=@IDProducto";


                        };
                        comandoM.Parameters.Add("@IDProducto", SqlDbType.VarChar, 10);
                        comandoM.Parameters["@IDProducto"].Value = IDP[x].ToString();
                        SqlDataReader read = comandoM.ExecuteReader();
                        while (read.Read())
                        {

                            Minimo1.Add(read[0].ToString());
                        }
                        read.Close();
                    }
                    //comando.ExecuteNonQuery();
                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;
        }
        public Boolean EliminarVentas(DatosVentas dato)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comando = new SqlCommand();
                    {
                        comando.Connection = conn;
                        comando.CommandText = "DELETE Ventas WHERE Folio=@Folio";

                    };
                    comando.Parameters.Add("@Folio", SqlDbType.VarChar, 10);
                    comando.Parameters["@Folio"].Value = dato.sFolio;


                    comando.ExecuteNonQuery();
                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;
        }
        public Boolean ConsultarVentas(DatosVentas dato, String sFolio, ref Boolean bExiste)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comando = new SqlCommand();
                    {
                        comando.Connection = conn;
                        comando.CommandText = "SELECT IDUsuario,Fecha,Descuento,Subtotal,IVA,Total FROM Ventas WHERE Folio=@Folio";

                    };
                    comando.Parameters.Add("@Folio", SqlDbType.VarChar, 10);

                    comando.Parameters["@Folio"].Value = dato.sFolio;

                    SqlDataReader reader = comando.ExecuteReader();
                    dato.sFolio = sFolio;
                    while (reader.Read())
                    {
                        bExiste = true;

                        dato.sIDUsuario = reader[0].ToString();
                        dato.Fecha = Convert.ToDateTime(reader[1].ToString());
                        dato.dDescuento = Convert.ToDouble(reader[2].ToString());
                        dato.dSubtotal = Convert.ToDouble(reader[3].ToString());
                        dato.dIVA = Convert.ToDouble(reader[4].ToString());
                        dato.dTotal = Convert.ToDouble(reader[5].ToString());

                    }


                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;

        }

        public static void AgregarRenglon(DatosVentasDetalle renglon)
        {

            detalleVenta.Add(renglon);
        }
        public DatosVentasDetalle GetRenglon(Int32 nIdex)
        {
            detalleVenta.Clear();
            DatosVentasDetalle renglon;
            if (detalleVenta.Count <= nIdex)

                renglon = (DatosVentasDetalle)detalleVenta[nIdex];
            else
                renglon = null;
            return renglon;

        }
        #endregion
        #region VentasDetalle
        public Boolean AltaVentasDetalle(DatosVentasDetalle dato)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comando = new SqlCommand();
                    {
                        comando.Connection = conn;
                        comando.CommandText = "INSERT INTO VentasDetalle (Folio,( Select IDProducto where CodigoBarras=@CodigoBarras),Cantidad,Medida,PrecioUnitario)" +
                            "VALUES (@Folio,@IDProductos,@Cantidad,@Medida,@Descuento,@PrecioVenta,@IVA)" +
                             "UPDATE Inventario SET Stock=Stock-Cantidad FROM VentasDetalle WHERE IDProducto=(Select IDProducto FROM Productos Where CodigoBarras=@CodigoBarras)";
                    };
                    comando.Parameters.Add("@Folio", SqlDbType.Int, 10);
                    comando.Parameters.Add("@IDProductos", SqlDbType.VarChar, 10);
                    comando.Parameters.Add("@Cantidad", SqlDbType.Float);
                    comando.Parameters.Add("@PrecioVenta", SqlDbType.Float);
                    comando.Parameters.Add("@CodigoBarras", SqlDbType.VarChar, 15);

                    comando.Parameters["@Folio"].Value = dato.sFolio;
                    comando.Parameters["@IDProductos"].Value = dato.sIDProducto;
                    comando.Parameters["@Cantidad"].Value = dato.dCantidad;
                    comando.Parameters["@PrecioVenta"].Value = dato.dPrecioUnitario;
                    comando.Parameters["@CodigoBarras"].Value = dato.sCodigoBarras;


                    comando.ExecuteNonQuery();
                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;

        }
        public Boolean ActualizarVentasDetalle(DatosVentasDetalle dato)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comando = new SqlCommand();
                    {
                        comando.Connection = conn;
                        comando.CommandText = "UPDATE VentasDetalle SET IDProductos=@IDProductos,Cantidad=@Cantidad,Descuento=@Descuento,PrecioVenta=@PrecioVenta,IVA=@IVA " +
                            "WHERE Folio=@Folio";

                    };
                    comando.Parameters.Add("Folio", SqlDbType.VarChar, 10);
                    comando.Parameters.Add("IDProducto", SqlDbType.Float, 10);
                    comando.Parameters.Add("Cantidad", SqlDbType.Date);
                    comando.Parameters.Add("PrecioVenta", SqlDbType.Float);


                    comando.Parameters["Folio"].Value = dato.sFolio;
                    comando.Parameters["IDProducto"].Value = dato.sIDProducto;
                    comando.Parameters["Cantidad"].Value = dato.dCantidad;
                    comando.Parameters["PrecioVenta"].Value = dato.dPrecioUnitario;


                    comando.ExecuteNonQuery();
                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;

        }
        public Boolean EliminarVentasDetalle(DatosVentasDetalle dato)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comando = new SqlCommand();
                    {
                        comando.Connection = conn;
                        comando.CommandText = "DELETE VentasDetalle WHERE Folio=@Folio";

                    };
                    comando.Parameters.Add("Folio", SqlDbType.VarChar, 10);
                    comando.Parameters["Folio"].Value = dato.sFolio;


                    comando.ExecuteNonQuery();
                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;
        }
        public Boolean ConsultarVentasDetalle(String sFolio, ref ArrayList ID, ref ArrayList Descripcion, ref ArrayList Cantidad, ref ArrayList Medida, ref ArrayList PrecioU, ref ArrayList Importe, ref Boolean bExiste)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comando = new SqlCommand();
                    {
                        comando.Connection = conn;
                        comando.CommandText = "SELECT IDProducto,Descripcion,Cantidad,Medida,PrecioUnitario,Importe FROM VentasDetalle WHERE Folio=@Folio";

                    };
                    comando.Parameters.Add("@Folio", SqlDbType.Int, 10);

                    comando.Parameters["@Folio"].Value = sFolio;

                    SqlDataReader reader = comando.ExecuteReader();

                    while (reader.Read())
                    {
                        bExiste = true;
                        //ato.iRenglon =Convert.ToInt16( reader[0].ToString());
                        ID.Add(reader[0].ToString());
                        Descripcion.Add(reader[1].ToString());
                        Cantidad.Add(Convert.ToDouble(reader[2].ToString()));
                        Medida.Add(reader[3].ToString());
                        PrecioU.Add(Convert.ToDouble(reader[4].ToString()));
                        Importe.Add(Convert.ToDouble(reader[5].ToString()));


                    }


                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;

        }

        #endregion
        public Boolean Folio(ref String sID)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
            {
                try
                {
                    conn.Open();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = conn,
                        CommandText = "SELECT  MAX(Folio)+1 FROM Ventas"
                    };
                    sID = command.ExecuteScalar().ToString();
                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            }
            return bAllok;
        }

        public Boolean Remover()
        {
            Boolean bAllok = false;
            return bAllok;
        }

        //    public Boolean ConsultaProductos(ref List<String> des)
        //    {
        //        Boolean bAllok = false;
        //        try
        //        {
        //            cnn.Open();

        //            SqlCommand comando = new SqlCommand
        //            {
        //                Connection = cnn,
        //                CommandText = "SELECT Descripcion FROM Productos" +
        //                $"WHERE (Descripcion) LIKE %@Descripcion%"
        //            };
        //            comando.Parameters["@Descripcion"].Value = DatosProductos.sDescripcion1;
        //            SqlDataReader reader = comando.ExecuteReader();

        //            while (reader.Read())
        //            {
        //               des.Add(reader[0].ToString());
        //            }

        //            cnn.Close();

        //            bAllok = true;
        //        }
        //        catch (Exception ex)
        //        {
        //            sLastError = ex.Message;
        //        }

        //        return bAllok;
        //    } 
        
    }
}
