﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReglasClientes
{
   public class DatosPresupuestosDetalle
    {
        public String sFolio;
        public int iRenglon;
        public String sIDProducto;
        public String sDescripcion;
        public Double dCantidad;
        public String sMedida;
        public Double dPrecioUnitario;
        public Double dImporte;
    }
}
