﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace ReglasClientes
{
    public class OperacionesPresupuestos
    {
        public String sLastError = "";

        public static ArrayList detalleVenta = new ArrayList();
        #region Ventas
        public Boolean AltaVentas(DatosPresupuestos dato, DatosPresupuestosDetalle detalle)
        {
            Boolean bAllok = false;

            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
            {
                SqlTransaction transaction = null;
                try
                {
                    conn.Open();
                    transaction = conn.BeginTransaction();

                    SqlCommand comando = new SqlCommand();
                    {
                        comando.Connection = conn;
                        comando.Transaction = transaction;
                        comando.CommandText = "INSERT INTO Presupuestos (Folio,IDUsuario,Fecha,Descuento,Subtotal,IVA,Total)" +
                            "VALUES (@Folio,@IDUsuario,@Fecha,@Descuento,@Subtotal,@IVA,@Total)";

                    };
                    comando.Parameters.Add("@Folio", SqlDbType.VarChar, 10);
                    comando.Parameters.Add("@IDUsuario", SqlDbType.VarChar, 10);
                    comando.Parameters.Add("@Fecha", SqlDbType.Date);
                    comando.Parameters.Add("@Descuento", SqlDbType.Float);
                    comando.Parameters.Add("@Subtotal", SqlDbType.Float);
                    comando.Parameters.Add("@IVA", SqlDbType.Float);
                    comando.Parameters.Add("@Total", SqlDbType.Float);

                    comando.Parameters["@Folio"].Value = dato.sFolio;
                    comando.Parameters["@IDUsuario"].Value = dato.sIDUsuario;
                    comando.Parameters["@Fecha"].Value = dato.Fecha;
                    comando.Parameters["@Descuento"].Value = dato.dDescuento;
                    comando.Parameters["@Subtotal"].Value = dato.dSubtotal;
                    comando.Parameters["@IVA"].Value = dato.dIVA;
                    comando.Parameters["@Total"].Value = dato.dTotal;

                    comando.ExecuteNonQuery();

                    foreach (DatosPresupuestosDetalle row in detalleVenta)                    
                    {
                        
                        SqlCommand sqlCommandDetalle = new SqlCommand();
                        sqlCommandDetalle.Transaction = transaction;
                        sqlCommandDetalle.Connection = conn;
                        sqlCommandDetalle.CommandText = "INSERT INTO PresupuestosDetalle (Folio,IDProducto,Descripcion,Cantidad,Medida,PrecioUnitario,Importe)" +
                          "VALUES (@Folio,(SELECT IDProducto FROM Productos Where Descripcion=@Descripcion),@Descripcion,@Cantidad,@Medida,@PrecioUnitario,@Importe)";


                        sqlCommandDetalle.Parameters.Add("@Folio", SqlDbType.VarChar, 10);
                        sqlCommandDetalle.Parameters.Add("@IDProducto", SqlDbType.VarChar, 10);
                        sqlCommandDetalle.Parameters.Add("@Descripcion", SqlDbType.VarChar, 100);
                        sqlCommandDetalle.Parameters.Add("@Medida", SqlDbType.VarChar, 10);
                        sqlCommandDetalle.Parameters.Add("@Cantidad", SqlDbType.Float);
                        sqlCommandDetalle.Parameters.Add("@PrecioUnitario", SqlDbType.Float);
                        sqlCommandDetalle.Parameters.Add("@Importe", SqlDbType.Float);


                        sqlCommandDetalle.Parameters["@Folio"].Value = row.sFolio;
                        sqlCommandDetalle.Parameters["@IDProducto"].Value = row.sIDProducto;
                        sqlCommandDetalle.Parameters["@Descripcion"].Value = row.sDescripcion;
                        sqlCommandDetalle.Parameters["@Cantidad"].Value = row.dCantidad;
                        sqlCommandDetalle.Parameters["@Medida"].Value = row.sMedida;
                        sqlCommandDetalle.Parameters["@PrecioUnitario"].Value = row.dPrecioUnitario;
                        sqlCommandDetalle.Parameters["@Importe"].Value = row.dImporte;
                        sqlCommandDetalle.ExecuteNonQuery();

                        
                    }
                    transaction.Commit();
                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                    transaction.Rollback();
                }
            }
            return bAllok;

        }
        public Boolean EliminarVentas(DatosPresupuestos dato)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comando = new SqlCommand();
                    {
                        comando.Connection = conn;
                        comando.CommandText = "DELETE Presupuestos WHERE Folio=@Folio";

                    };
                    comando.Parameters.Add("@Folio", SqlDbType.VarChar, 10);
                    comando.Parameters["@Folio"].Value = dato.sFolio;


                    comando.ExecuteNonQuery();
                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;
        }
        public Boolean ConsultarVentas(DatosPresupuestos dato, String sFolio, ref Boolean bExiste)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comando = new SqlCommand();
                    {
                        comando.Connection = conn;
                        comando.CommandText = "SELECT IDUsuario,Fecha,Descuento,Subtotal,IVA,Total FROM Presupuestos WHERE Folio=@Folio";

                    };
                    comando.Parameters.Add("@Folio", SqlDbType.VarChar, 10);

                    comando.Parameters["@Folio"].Value = dato.sFolio;

                    SqlDataReader reader = comando.ExecuteReader();
                    dato.sFolio = sFolio;
                    while (reader.Read())
                    {
                        bExiste = true;

                        dato.sIDUsuario = reader[0].ToString();
                        dato.Fecha = Convert.ToDateTime(reader[1].ToString());
                        dato.dDescuento = Convert.ToDouble(reader[2].ToString());
                        dato.dSubtotal = Convert.ToDouble(reader[3].ToString());
                        dato.dIVA = Convert.ToDouble(reader[4].ToString());
                        dato.dTotal = Convert.ToDouble(reader[5].ToString());

                    }


                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;

        }
        public static void AgregarRenglon(DatosPresupuestosDetalle renglon)
        {
            detalleVenta.Add(renglon);
        }
        public DatosPresupuestosDetalle GetRenglon(Int32 nIdex)
        {
            DatosPresupuestosDetalle renglon;
            if (detalleVenta.Count <= nIdex)

                renglon = (DatosPresupuestosDetalle)detalleVenta[nIdex];
            else
                renglon = null;
            return renglon;

        }
        #endregion
        #region VentasDetalle
       
     
        public Boolean ActualizarVentasDetalle(DatosPresupuestosDetalle dato)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comando = new SqlCommand();
                    {
                        comando.Connection = conn;
                        comando.CommandText = "UPDATE PresupuestosDetalle SET IDProductos=@IDProductos,Cantidad=@Cantidad,Descuento=@Descuento,PrecioVenta=@PrecioVenta,IVA=@IVA " +
                            "WHERE Folio=@Folio";

                    };
                    comando.Parameters.Add("Folio", SqlDbType.VarChar, 10);
                    comando.Parameters.Add("IDProducto", SqlDbType.Float, 10);
                    comando.Parameters.Add("Cantidad", SqlDbType.Date);
                    comando.Parameters.Add("PrecioVenta", SqlDbType.Float);


                    comando.Parameters["Folio"].Value = dato.sFolio;
                    comando.Parameters["IDProducto"].Value = dato.sIDProducto;
                    comando.Parameters["Cantidad"].Value = dato.dCantidad;
                    comando.Parameters["PrecioVenta"].Value = dato.dPrecioUnitario;


                    comando.ExecuteNonQuery();
                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;

        }
        public Boolean EliminarVentasDetalle(DatosPresupuestosDetalle dato)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comando = new SqlCommand();
                    {
                        comando.Connection = conn;
                        comando.CommandText = "DELETE PresupuestosDetalle WHERE Folio=@Folio";

                    };
                    comando.Parameters.Add("Folio", SqlDbType.VarChar, 10);
                    comando.Parameters["Folio"].Value = dato.sFolio;


                    comando.ExecuteNonQuery();
                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;
        }
        public Boolean ConsultarVentasDetalle(String sFolio, ref ArrayList ID, ref ArrayList Descripcion, ref ArrayList Cantidad, ref ArrayList Medida, ref ArrayList PrecioU, ref ArrayList Importe, ref Boolean bExiste)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comando = new SqlCommand();
                    {
                        comando.Connection = conn;
                        comando.CommandText = "SELECT IDProducto,Descripcion,Cantidad,Medida,PrecioUnitario,Importe FROM PresupuestosDetalle WHERE Folio=@Folio";

                    };
                    comando.Parameters.Add("@Folio", SqlDbType.Int, 10);

                    comando.Parameters["@Folio"].Value = sFolio;

                    SqlDataReader reader = comando.ExecuteReader();

                    while (reader.Read())
                    {
                        bExiste = true;
                       
                        ID.Add(reader[0].ToString());
                        Descripcion.Add(reader[1].ToString());
                        Cantidad.Add(Convert.ToDouble(reader[2].ToString()));
                        Medida.Add(reader[3].ToString());
                        PrecioU.Add(Convert.ToDouble(reader[4].ToString()));
                        Importe.Add(Convert.ToDouble(reader[5].ToString()));


                    }


                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;

        }

        #endregion
        public Boolean Folio(ref String sID)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
            {
                try
                {
                    conn.Open();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = conn,
                        CommandText = "SELECT  MAX(Folio)+1 FROM Presupuestos"
                    };
                    sID = command.ExecuteScalar().ToString();
                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            }
            return bAllok;
        }

        public Boolean Remover()
        {
            Boolean bAllok = false;
            return bAllok;
        }


    }
}

