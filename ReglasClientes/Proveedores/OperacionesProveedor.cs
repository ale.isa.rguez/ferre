﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace ReglasClientes
{
    public class OperacionesProveedor
    {

        public String sLastError = "";
        public String sServidor;
        public String sBaseDatos;
        public String sUsuario;
        public String sPassword;

        public OperacionesProveedor()
        {
            this.sServidor = sServidor;
            this.sBaseDatos = sBaseDatos;
            this.sUsuario = sUsuario;
            this.sPassword = sPassword;
        }
        public Boolean AltaProveedor(DatosProveedor Dato)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comand = new SqlCommand();
                    {
                        comand.Connection = conn;
                        comand.CommandText = "INSERT INTO Proveedores(RFC,Nombre,Telefono,CorreoElectronico)" +
                            "VALUES(@RFC,@Nombre,@Telefono,@CorreoElectronico)";
                    };
                    comand.Parameters.Add("@RFC", SqlDbType.VarChar, 13);
                    comand.Parameters.Add("@Nombre", SqlDbType.VarChar, 50);
                    comand.Parameters.Add("@Telefono", SqlDbType.VarChar, 50);
                    comand.Parameters.Add("@CorreoElectronico", SqlDbType.VarChar, 100);


                    comand.Parameters["@RFC"].Value = Dato.sRFC;
                    comand.Parameters["@Nombre"].Value = Dato.sNombre;
                    comand.Parameters["@Telefono"].Value = Dato.sTel;
                    comand.Parameters["@CorreoElectronico"].Value = Dato.sCorreo;
                   

                    comand.ExecuteNonQuery();
                    conn.Close();
                    bAllok = true;

                }
                catch(Exception ex)
                {
                    sLastError = ex.Message;
                }
                return bAllok;
        }
        public Boolean ActvualizarProveedor(DatosProveedor Dato)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comand = new SqlCommand();
                    {
                        comand.Connection = conn;
                        comand.CommandText = "UPDATE Proveedores SET Nombre=@Nombre,Telefono=@Telefono,CorreoElectronico=@CorreoElectronico WHERE RFC=@RFC";
                         
                    };
                    comand.Parameters.Add("@RFC", SqlDbType.VarChar, 13);
                    comand.Parameters.Add("@Nombre", SqlDbType.VarChar, 50);
                    comand.Parameters.Add("@Telefono", SqlDbType.VarChar, 50);
                    comand.Parameters.Add("@CorreoElectronico", SqlDbType.VarChar, 100);


                    comand.Parameters["@RFC"].Value = Dato.sRFC;
                    comand.Parameters["@Nombre"].Value = Dato.sNombre;
                    comand.Parameters["@Telefono"].Value = Dato.sTel;
                    comand.Parameters["@CorreoElectronico"].Value = Dato.sCorreo;
                    

                    comand.ExecuteNonQuery();
                    conn.Close();
                    bAllok = true;

                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;
            
        }
        public Boolean EliminarProveedor(DatosProveedor Dato)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comand = new SqlCommand();
                    {
                        comand.Connection = conn;
                        comand.CommandText = "DELETE Proveedores WHERE RFC= @RFC";

                    };
                    comand.Parameters.Add("@RFC", SqlDbType.VarChar, 10);                    

                    comand.Parameters["@RFC"].Value = Dato.sRFC;
                    

                    comand.ExecuteNonQuery();
                    conn.Close();
                    bAllok = true;

                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;
           
        }
        public Boolean ConsultarProveedor(ref DatosProveedor Dato, String sID,ref Boolean bExite)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comand = new SqlCommand();
                    {
                        comand.Connection = conn;
                        comand.CommandText = "SELECT Nombre,Telefono,CorreoElectronico,RFC FROM Proveedores WHERE RFC= @RFC";

                    };
                    comand.Parameters.Add("@RFC", SqlDbType.VarChar, 13);

                    comand.Parameters["@RFC"].Value = Dato.sRFC;
                    SqlDataReader reader =comand.ExecuteReader();
                    Dato.sRFC = sID;
                    while (reader.Read())
                    {
                        bExite = true;
                        Dato.sNombre = reader[0].ToString();
                        Dato.sTel = reader[1].ToString();
                        Dato.sCorreo = reader[2].ToString();
                      
                    }
                    //comand.ExecuteNonQuery();
                    conn.Close();
                    bAllok = true;

                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;
            
        }
        public Boolean ConsultarProveedorCB(ref DatosProveedor Dato, String sID, ref Boolean bExite)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comand = new SqlCommand();
                    {
                        comand.Connection = conn;
                        comand.CommandText = "SELECT CorreoElectronico,Telefono,RFC FROM Proveedores WHERE Nombre= @Nombre";

                    };
                    comand.Parameters.Add("@Nombre", SqlDbType.VarChar, 13);

                    comand.Parameters["@Nombre"].Value = Dato.sNombre;
                    SqlDataReader reader = comand.ExecuteReader();
                    Dato.sRFC = sID;
                    while (reader.Read())
                    {
                        bExite = true;
                      
                        
                        Dato.sCorreo = reader[0].ToString();
                        Dato.sTel = reader[1].ToString();
                        Dato.sRFC = reader[2].ToString();

                    }
                    
                    conn.Close();
                    bAllok = true;

                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;

        }
    }
}
