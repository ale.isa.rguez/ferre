﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Collections;


namespace ReglasClientes
{
   public  class Reporte
    {
        public String sLastError = "";
        public Boolean Vista()
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
            {
                SqlTransaction transaction = null;
                try
                {
                    conn.Open();
                    transaction = conn.BeginTransaction();
                    SqlCommand sqlCommandDetalle = new SqlCommand();
                    {
                        sqlCommandDetalle.Connection = conn;
                        sqlCommandDetalle.Transaction = transaction;
                        sqlCommandDetalle.CommandText = "";

                    };
                   // sqlCommandDetalle.Parameters.Add("@Folio", SqlDbType.VarChar, 10);
                   // sqlCommandDetalle.Parameters["@Folio"].Value;

                    sqlCommandDetalle.ExecuteNonQuery();

                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Transaction = transaction;
                    sqlCommand.Connection = conn;
                    sqlCommand.CommandText = "EXECUTE EliminarInventarioCompras @Folio";

                    sqlCommand.Parameters.Add("@Folio", SqlDbType.VarChar, 10);
                    sqlCommand.Parameters["@Folio"].Value = 

                    sqlCommand.ExecuteNonQuery();

                    transaction.Commit();
                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    sLastError = ex.Message;
                }
            }
            return bAllok;
        }
    }
}
