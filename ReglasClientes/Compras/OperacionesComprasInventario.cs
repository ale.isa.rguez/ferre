﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace ReglasClientes
{
    public class OperacionesComprasInventario
    {
        public String sLastError = "";
        public String sServidor;
        public String sBaseDatos;
        public String sUsuario;
        public String sPassword;

        public OperacionesComprasInventario()
        {
            this.sServidor = sServidor;
            this.sBaseDatos = sBaseDatos;
            this.sUsuario = sUsuario;
            this.sPassword = sPassword;
        }
        public static ArrayList detalleCompra = new ArrayList();

        public Boolean Altacompras(DatosCompras dato, DatosComprasDetalle detalle, DatosProveedor datoProv)
        {
            Boolean bAllok = false;

            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
            {
                SqlTransaction transaction = null;
                try
                {
                    conn.Open();
                    transaction = conn.BeginTransaction();

                    SqlCommand comando = new SqlCommand();
                    {
                        comando.Connection = conn;
                        comando.Transaction = transaction;
                        comando.CommandText = "INSERT INTO InventarioCompras (Folio,RFC,Fecha,Descuento,Subtotal,IVA,Total)" +
                            "VALUES (@Folio,(SELECT RFC FROM Proveedores WHERE Nombre=@Nombre),@Fecha,@Descuento,@Subtotal,@IVA,@Total)";

                    };
                    comando.Parameters.Add("@Folio", SqlDbType.VarChar, 10);
                    comando.Parameters.Add("@Nombre", SqlDbType.VarChar, 50);
                    comando.Parameters.Add("@Fecha", SqlDbType.Date);
                    comando.Parameters.Add("@Descuento", SqlDbType.Float);
                    comando.Parameters.Add("@Subtotal", SqlDbType.Float);
                    comando.Parameters.Add("@IVA", SqlDbType.Float);
                    comando.Parameters.Add("@Total", SqlDbType.Float);

                    comando.Parameters["@Folio"].Value = dato.sFolio;
                    comando.Parameters["@Nombre"].Value = datoProv.sNombre;
                    comando.Parameters["@Fecha"].Value = dato.dFecha;
                    comando.Parameters["@Descuento"].Value = dato.dDescuento;
                    comando.Parameters["@Subtotal"].Value = dato.dSubtotal;
                    comando.Parameters["@IVA"].Value = dato.dIVA;
                    comando.Parameters["@Total"].Value = dato.dTotal;

                    comando.ExecuteNonQuery();

                    foreach (DatosComprasDetalle row in detalleCompra)                    
                    {
                       
                        SqlCommand sqlCommandDetalle = new SqlCommand();
                        sqlCommandDetalle.Transaction = transaction;
                        sqlCommandDetalle.Connection = conn;
                        sqlCommandDetalle.CommandText = "Insert into InventarioComprasDetalle values(@Folio,(SELECT IDProducto FROM Productos Where CodigoBarras=@CodigoBarras),@Descripcion,@Cantidad,@Medida,@PrecioCompra,@Importe)";
                       

                        sqlCommandDetalle.Parameters.Add("@Folio", SqlDbType.VarChar, 10);
                        sqlCommandDetalle.Parameters.Add("@CodigoBarras", SqlDbType.VarChar, 15);
                        sqlCommandDetalle.Parameters.Add("@Descripcion", SqlDbType.VarChar, 100);
                        sqlCommandDetalle.Parameters.Add("@Cantidad", SqlDbType.Float);
                        sqlCommandDetalle.Parameters.Add("@Medida", SqlDbType.VarChar, 10);
                        sqlCommandDetalle.Parameters.Add("@PrecioCompra", SqlDbType.Float);
                        sqlCommandDetalle.Parameters.Add("@Importe", SqlDbType.Float);


                        sqlCommandDetalle.Parameters["@Folio"].Value = row.sFolio;
                        sqlCommandDetalle.Parameters["@CodigoBarras"].Value = row.sIDProducto;
                        sqlCommandDetalle.Parameters["@Descripcion"].Value = row.sDescripcion;
                        sqlCommandDetalle.Parameters["@Cantidad"].Value = row.dCantidad;
                        sqlCommandDetalle.Parameters["@Medida"].Value = row.sMedida;
                        sqlCommandDetalle.Parameters["@PrecioCompra"].Value = row.dPrecio;
                        sqlCommandDetalle.Parameters["@Importe"].Value = row.dImporte;
                        sqlCommandDetalle.ExecuteNonQuery();
                                               
                    }
                    transaction.Commit();
                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                    transaction.Rollback();
                }
            }
            return bAllok;

        }
        public Boolean EliminarCompras( DatosComprasDetalle detalle, DatosCompras dato)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
            {    SqlTransaction transaction = null;
                try
                {
                    conn.Open();
                    transaction = conn.BeginTransaction();
                    SqlCommand sqlCommandDetalle = new SqlCommand();
                    {
                        sqlCommandDetalle.Connection = conn;
                       sqlCommandDetalle.Transaction = transaction;
                        sqlCommandDetalle.CommandText = "EXECUTE EliminarInventarioComprasDetalle @Folio";

                    };
                    sqlCommandDetalle.Parameters.Add("@Folio", SqlDbType.VarChar, 10);
                    sqlCommandDetalle.Parameters["@Folio"].Value = detalle.sFolio;

                    sqlCommandDetalle.ExecuteNonQuery();

                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Transaction = transaction;
                    sqlCommand.Connection = conn;
                    sqlCommand.CommandText = "EXECUTE EliminarInventarioCompras @Folio";

                    sqlCommand.Parameters.Add("@Folio", SqlDbType.VarChar, 10);
                    sqlCommand.Parameters["@Folio"].Value = dato.sFolio;

                    sqlCommand.ExecuteNonQuery();

                    transaction.Commit();
                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    sLastError = ex.Message;
                }
            }
            return bAllok;
        }
        public Boolean ConsultarCompras(DatosCompras dato, String sFolio, ref Boolean bExiste)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comando = new SqlCommand();
                    {
                        comando.Connection = conn;
                        comando.CommandText = "EXECUTE ConsultarCompras @Folio";

                    };
                    comando.Parameters.Add("@Folio", SqlDbType.VarChar, 10);

                    comando.Parameters["@Folio"].Value = dato.sFolio;

                    SqlDataReader reader = comando.ExecuteReader();
                    dato.sFolio = sFolio;
                    while (reader.Read())
                    {
                        bExiste = true;

                        dato.sIDProveedor = reader[0].ToString();
                        dato.dFecha = Convert.ToDateTime(reader[1].ToString());
                        dato.dDescuento = Convert.ToDouble(reader[2].ToString());
                        dato.dSubtotal = Convert.ToDouble(reader[3].ToString());
                        dato.dIVA = Convert.ToDouble(reader[4].ToString());
                        dato.dTotal = Convert.ToDouble(reader[5].ToString());

                    }


                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;

        }
        public static void AgregarRenglon(DatosComprasDetalle renglon)
        {
           
            detalleCompra.Add(renglon);
        }           
        public DatosComprasDetalle GetRenglon(Int32 nIndex)
        {
            DatosComprasDetalle renglon;
            if (detalleCompra.Count <= nIndex)
                renglon = (DatosComprasDetalle)detalleCompra[nIndex];
            else
                renglon =null;
            return renglon;
        }      
        public Boolean ConsultarComprasDetalle(String sFolio, ref ArrayList ID, ref ArrayList Descripcion, ref ArrayList Cantidad,ref ArrayList Medida, ref ArrayList PrecioC, ref ArrayList Importe)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comando = new SqlCommand();
                    {
                        comando.Connection = conn;
                        comando.CommandText = "EXECUTE ConsultarComprasDetalle @Folio";


                    };
                    comando.Parameters.Add("@Folio", SqlDbType.VarChar, 10);

                    comando.Parameters["@Folio"].Value = sFolio;
                    
                    SqlDataReader reader = comando.ExecuteReader();

                    while (reader.Read())
                    {

                        ID.Add(reader[0].ToString());
                        Descripcion.Add(reader[1].ToString());
                        Cantidad.Add(Convert.ToDouble(reader[2].ToString()));
                        Medida.Add(reader[3].ToString());
                        PrecioC.Add(Convert.ToDouble(reader[4].ToString()));
                        Importe.Add(Convert.ToDouble(reader[5].ToString()));


                    }


                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;

        }
        public Boolean Folio(ref String sID)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
            {
                try
                {
                    conn.Open();
                    SqlCommand command = new SqlCommand
                    {
                        Connection = conn,
                        CommandText = "SELECT  MAX(Folio)+1 FROM InventarioCompras"
                    };
                    sID = command.ExecuteScalar().ToString();
                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            }
            return bAllok;
        }

        public Boolean ConsultarID(ref DatosProductos dato, String sID, ref Boolean bExiste)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comand = new SqlCommand();
                    {
                        comand.Connection = conn;
                        comand.CommandText = "SELECT Descripcion,Unidad FROM Productos WHERE IDProducto=(Select IDProducto FROM Productos Where CodigoBarras=@CodigoBarras)";

                    };

                    comand.Parameters.Add("@CodigoBarras", SqlDbType.VarChar, 100);
                    comand.Parameters["@CodigoBarras"].Value = dato.sIDProducto;

                    SqlDataReader reader = comand.ExecuteReader();
                    dato.sIDProducto = sID;
                    while (reader.Read())
                    {
                        bExiste = true;
                        dato.sDescripcion = reader[0].ToString();                       
                        dato.sUnidad = reader[1].ToString();


                    }
                    conn.Close();
                    bAllok = true;

                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;
        }

    }
}
