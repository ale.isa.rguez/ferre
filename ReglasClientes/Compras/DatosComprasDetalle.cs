﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReglasClientes
{
   public class DatosComprasDetalle
    {
        public String sFolio;
        public String sIDProducto;
        public String sDescripcion;
        public Double dCantidad;
        public String sMedida;
        public Double dPrecio;
        public Double dImporte;
    }
}
