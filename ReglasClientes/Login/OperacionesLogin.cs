﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace ReglasClientes
{
   public class OperacionesLogin
    {
        static SqlConnection cnn = new SqlConnection(OperacionesProductos.SCadenaConexion);
        static SqlCommand comando;
        static DataSet ds;
        static DataTable data;
        static DataTable DtVacio = new DataTable();

        public static DataTable ConsultaTablas(string consulta)
        {
            try
            {
                cnn.Open();
                comando = new SqlCommand(consulta, cnn);
                comando.ExecuteNonQuery();
                SqlDataAdapter sdq = new SqlDataAdapter();
                sdq.SelectCommand = comando;
                ds = new DataSet();
                sdq.Fill(ds);
                data = new DataTable();
                data = ds.Tables[0];
                cnn.Close();
                return data;
            }
            catch
            {
                return DtVacio;
            }
        }
    }
}
