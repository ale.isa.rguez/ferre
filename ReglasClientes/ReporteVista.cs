﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Collections;
using System.Data;

namespace ReglasClientes
{
    public class ReporteVista
    {
        public String sLastError = "";

       

        public DataTable Vista()
        {
            Boolean bAllok = false;
            DataTable tabla = new DataTable();


            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
            {
                try
                {
                    conn.Open();
                    SqlCommand command = new SqlCommand();

                    command.Connection = conn;
                    command.CommandText = "  SELECT dbo.Productos.IDProducto, dbo.Productos.Descripcion, dbo.VentasDetalle.Cantidad, dbo.Productos.PrecioPublico, dbo.Proveedores.RFC, dbo.Proveedores.Nombre, dbo.Ventas.Subtotal, dbo.Ventas.Total, dbo.Ventas.Fecha, dbo.Ventas.Folio" +
                                          "  FROM dbo.Productos INNER JOIN " +
                         " dbo.ProveedorProducto ON dbo.Productos.IDProducto = dbo.ProveedorProducto.IDProducto INNER JOIN " +
                         " dbo.Proveedores ON dbo.ProveedorProducto.RFC = dbo.Proveedores.RFC INNER JOIN " +
                         " dbo.VentasDetalle ON dbo.Productos.IDProducto = dbo.VentasDetalle.IDProducto INNER JOIN " +
                         " dbo.Ventas ON dbo.VentasDetalle.Folio = dbo.Ventas.Folio " +
                         "WHERE Ventas.Folio = VentasDetalle.Folio ";

                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(tabla);
                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
                return tabla;
            }
        }
    }
}
