﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace ReglasClientes
{
    public class OperacionesProductos
    {
        public String sLastError = "";
        public String sServidor;
        public String sBaseDatos;
        public String sUsuario;
        public String sPassword;
        public static String SCadenaConexion = @"Data Source=localhost;Initial Catalog=Ferreteria1;User ID=sa;Password=ale123";
       // public static String SCadenaConexion = @"Data Source = localhost;Initial Catalog = Ferreteria; User ID = sa;Password=ale123";

        public OperacionesProductos()
        {
            this.sServidor = sServidor;
            this.sBaseDatos = sBaseDatos;
            this.sUsuario = sUsuario;
            this.sPassword = sPassword;
        }
        public Boolean AltaPruducto(DatosProductos dato, DatosProveedor datosProveedor,DatosInventario datosInventario)
        {
           // DatosProveedor datosProveedor = new DatosProveedor();

            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(SCadenaConexion))

            {
                SqlTransaction transaction = null;
                try
                {
                   
                    conn.Open();
                    transaction = conn.BeginTransaction();
                    SqlCommand comand = new SqlCommand();
                    {
                        comand.Connection = conn;
                        comand.Transaction = transaction;
                        comand.CommandText = "INSERT INTO Productos(IDProducto,Descripcion,PrecioMayoreo,PrecioFrecuente,PrecioPublico,Minimo,Unidad,CodigoBarras)" +
                            "VALUES (@IDProducto,@Descripcion,@PrecioMayoreo,@PrecioFrecuente,@PrecioPublico,@Minimo,@Unidad,@CodigoBarras)";                         

                    };

                    comand.Parameters.Add("@IDProducto", SqlDbType.VarChar, 10);
                    comand.Parameters.Add("@Descripcion", SqlDbType.VarChar, 100);                    
                    comand.Parameters.Add("@PrecioMayoreo", SqlDbType.Float);
                    comand.Parameters.Add("@PrecioFrecuente", SqlDbType.Float);
                    comand.Parameters.Add("@PrecioPublico", SqlDbType.Float);
                    comand.Parameters.Add("@Minimo", SqlDbType.Float);
                    comand.Parameters.Add("@Unidad", SqlDbType.VarChar,10);
                    comand.Parameters.Add("@CodigoBarras", SqlDbType.VarChar, 15);

                    comand.Parameters["@IDProducto"].Value = dato.sIDProducto;
                    comand.Parameters["@Descripcion"].Value = dato.sDescripcion;                   
                    comand.Parameters["@PrecioMayoreo"].Value = dato.dPrecioMayoreo;
                    comand.Parameters["@PrecioFrecuente"].Value = dato.dPrecioFrecuente;
                    comand.Parameters["@PrecioPublico"].Value = dato.dPrecioPublico;
                    comand.Parameters["@Minimo"].Value = dato.dMinimo;
                    comand.Parameters["@Unidad"].Value = dato.sUnidad;
                    comand.Parameters["@CodigoBarras"].Value = dato.sCodigoBarras;


                    comand.ExecuteNonQuery();
                    SqlCommand InsertInventario = new SqlCommand();
                    {
                        InsertInventario.Connection = conn;
                        InsertInventario.Transaction = transaction;
                        InsertInventario.CommandText = "INSERT INTO Inventario (IDProducto,Descripcion,Stock,Medida) VALUES(@IDProducto,@Descripcion,@Stock,@Medida)";
                    };
                    InsertInventario.Parameters.Add("@IDProducto", SqlDbType.VarChar, 10);
                    InsertInventario.Parameters.Add("@Descripcion", SqlDbType.VarChar, 100);
                    InsertInventario.Parameters.Add("@Stock", SqlDbType.Float);
                    InsertInventario.Parameters.Add("@Medida", SqlDbType.VarChar,10);

                    InsertInventario.Parameters["@IDProducto"].Value = dato.sIDProducto;
                    InsertInventario.Parameters["@Stock"].Value = datosInventario.dStock;
                    InsertInventario.Parameters["@Descripcion"].Value = datosInventario.sDescripcion;
                    InsertInventario.Parameters["@Medida"].Value = datosInventario.SMedida;
                    InsertInventario.ExecuteNonQuery();


                    SqlCommand InsertProvProd = new SqlCommand();
                    {
                        InsertProvProd.Connection = conn;
                        InsertProvProd.Transaction = transaction;
                        InsertProvProd.CommandText = "INSERT INTO ProveedorProducto (IDProducto,RFC) VALUES (@IDProducto,(SELECT RFC FROM Proveedores WHERE Nombre=@Nombre))";
                    };
                    InsertProvProd.Parameters.Add("@IDProducto", SqlDbType.VarChar, 10);
                    InsertProvProd.Parameters.Add("@RFC", SqlDbType.VarChar, 13);
                    InsertProvProd.Parameters.Add("@Nombre", SqlDbType.VarChar, 50);


                    InsertProvProd.Parameters["@IDProducto"].Value = dato.sIDProducto;
                    InsertProvProd.Parameters["@RFC"].Value = datosProveedor.sRFC;
                    InsertProvProd.Parameters["@Nombre"].Value = datosProveedor.sNombre;

                    InsertProvProd.ExecuteNonQuery();

                    transaction.Commit();
                    conn.Close();
                    bAllok = true;
                    



                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    sLastError = ex.Message;
                }
            }
            return bAllok;
        }
        public Boolean ProdProv(DatosProductos dato,DatosProveedor datosProveedor)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(SCadenaConexion))
                try
                {
                    conn.Open();


                    SqlCommand InsertProvProd = new SqlCommand();
                    {
                        InsertProvProd.Connection = conn;
                        // InsertProvProd.Transaction = transaction;
                        InsertProvProd.CommandText = "INSERT INTO ProveedorProducto (IDProducto,RFC) VALUES (@IDProducto,(SELECT RFC FROM Proveedores WHERE Nombre=@Nombre))";
                    };
                    InsertProvProd.Parameters.Add("@IDProducto", SqlDbType.VarChar, 10);
                    InsertProvProd.Parameters.Add("@RFC", SqlDbType.VarChar, 13);
                    InsertProvProd.Parameters.Add("@Nombre", SqlDbType.VarChar, 50);


                    InsertProvProd.Parameters["@IDProducto"].Value = dato.sIDProducto;
                    InsertProvProd.Parameters["@RFC"].Value = datosProveedor.sRFC;
                    InsertProvProd.Parameters["@Nombre"].Value = datosProveedor.sNombre;

                    InsertProvProd.ExecuteNonQuery();
                    conn.Close();
                    bAllok = true;

                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;
        }
        public Boolean Actualizar(DatosProductos dato )
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(SCadenaConexion))
                try
                {
                    conn.Open();

                    SqlCommand comand = new SqlCommand();
                    {
                        comand.Connection = conn;
                        comand.CommandText = "UPDATE Productos SET Descripcion=@Descripcion,PrecioMayoreo=@PrecioMayoreo,PrecioFrecuente=@PrecioFrecuente,PrecioPublico=@PrecioPublico,Minimo=@Minimo,Unidad=@Unidad, CodigoBarras=@CodigoBarras WHERE IDProducto=@IDProducto";
                    };

                    comand.Parameters.Add("@IDProducto", SqlDbType.VarChar, 10);
                    comand.Parameters.Add("@Descripcion", SqlDbType.VarChar, 50);
                    comand.Parameters.Add("@PrecioMayoreo", SqlDbType.Float);
                    comand.Parameters.Add("@PrecioFrecuente", SqlDbType.Float);
                    comand.Parameters.Add("@PrecioPublico", SqlDbType.Float);
                    comand.Parameters.Add("@Minimo", SqlDbType.Float);
                    comand.Parameters.Add("@Unidad", SqlDbType.VarChar,10);
                    comand.Parameters.Add("@CodigoBarras", SqlDbType.VarChar, 15);

                    comand.Parameters["@IDProducto"].Value = dato.sIDProducto;
                    comand.Parameters["@Descripcion"].Value = dato.sDescripcion;                   
                    comand.Parameters["@PrecioMayoreo"].Value = dato.dPrecioMayoreo;
                    comand.Parameters["@PrecioFrecuente"].Value = dato.dPrecioFrecuente;
                    comand.Parameters["@PrecioPublico"].Value = dato.dPrecioPublico;
                    comand.Parameters["@Minimo"].Value = dato.dMinimo;
                    comand.Parameters["@Unidad"].Value = dato.sUnidad;
                    comand.Parameters["@CodigoBarras"].Value = dato.sCodigoBarras;

                    comand.ExecuteNonQuery();

                    
                    conn.Close();
                    bAllok = true;

                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;
        }
        public Boolean Eliminar(String dato)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(SCadenaConexion))
                try
                {
                    conn.Open();

                    SqlCommand comand = new SqlCommand();
                    {
                        comand.Connection = conn;
                        comand.CommandText = "DELETE FROM Productos  WHERE IDProducto=@IDProducto";
                    };

                    comand.Parameters.Add("@IDProducto", SqlDbType.VarChar, 10);

                    comand.Parameters["@IDProducto"].Value = dato;

                    comand.ExecuteNonQuery();
                    conn.Close();
                    bAllok = true;

                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;
        }
        public Boolean Consultar(ref DatosProductos dato, String sID, ref Boolean bExiste)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comand = new SqlCommand();
                    {
                        comand.Connection = conn;
                        comand.CommandText = "SELECT Descripcion,PrecioMayoreo,PrecioFrecuente,PrecioPublico,Minimo,Unidad,CodigoBarras FROM Productos WHERE IDProducto=@IDProducto";

                    };

                    comand.Parameters.Add("@IDProducto", SqlDbType.VarChar, 10);
                    comand.Parameters["@IDProducto"].Value = dato.sIDProducto;

                    SqlDataReader reader = comand.ExecuteReader();
                    dato.sIDProducto = sID;
                    while (reader.Read())
                    {
                        bExiste = true;
                        dato.sDescripcion = reader[0].ToString();
                        dato.dPrecioMayoreo = Convert.ToDouble(reader[1].ToString());
                        dato.dPrecioFrecuente= Convert.ToDouble(reader[2].ToString());
                        dato.dPrecioPublico = Convert.ToDouble(reader[3].ToString());
                        dato.dMinimo = Convert.ToDouble(reader[4].ToString());
                        dato.sUnidad = reader[5].ToString();
                        dato.sCodigoBarras = reader[6].ToString();

                    }
                    conn.Close();
                    bAllok = true;

                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;
        }
        public Boolean Consultar1(ref DatosProductos dato, String sID, ref Boolean bExiste)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comand = new SqlCommand();
                    {
                        comand.Connection = conn;
                        comand.CommandText = "SELECT Descripcion,PrecioMayoreo,PrecioFrecuente,PrecioPublico,Minimo,Unidad,IDProducto FROM Productos WHERE IDProducto=(Select IDProducto FROM Productos Where CodigoBarras=@CodigoBarras)";

                    };

                    comand.Parameters.Add("@CodigoBarras", SqlDbType.VarChar, 15);
                    comand.Parameters["@CodigoBarras"].Value = dato.sCodigoBarras;

                    SqlDataReader reader = comand.ExecuteReader();
                    dato.sCodigoBarras = sID;
                    while (reader.Read())
                    {
                        bExiste = true;
                        dato.sDescripcion = reader[0].ToString();
                        dato.dPrecioMayoreo = Convert.ToDouble(reader[1].ToString());
                        dato.dPrecioFrecuente = Convert.ToDouble(reader[2].ToString());
                        dato.dPrecioPublico = Convert.ToDouble(reader[3].ToString());
                        dato.dMinimo = Convert.ToDouble(reader[4].ToString());
                        dato.sUnidad = reader[5].ToString();
                        dato.sIDProducto = reader[6].ToString();

                    }
                    conn.Close();
                    bAllok = true;

                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;
        }
        public Boolean ConsultarCB(ref DatosProductos dato, String sID, ref Boolean bExiste)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comand = new SqlCommand();
                    {
                        comand.Connection = conn;
                        comand.CommandText = "SELECT CodigoBarras,PrecioMayoreo,PrecioFrecuente,PrecioPublico,Unidad FROM Productos WHERE Descripcion=@Descripcion";

                    };

                    comand.Parameters.Add("@Descripcion", SqlDbType.VarChar, 100);
                    comand.Parameters["@Descripcion"].Value = dato.sDescripcion;

                    SqlDataReader reader = comand.ExecuteReader();
                    dato.sIDProducto = sID;
                    while (reader.Read())
                    {
                        bExiste = true;
                        dato.sIDProducto = reader[0].ToString();
                        dato.dPrecioMayoreo = Convert.ToDouble(reader[1].ToString());
                        dato.dPrecioFrecuente = Convert.ToDouble(reader[2].ToString());
                        dato.dPrecioPublico = Convert.ToDouble(reader[3].ToString());
                        dato.sUnidad = reader[4].ToString();
                    

                    }
                    conn.Close();
                    bAllok = true;

                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;
        }

        public Boolean ConsultarCB1(ref DatosProductos dato, String sID, ref Boolean bExiste)
        {
            Boolean bAllok = false;
            using (SqlConnection conn = new SqlConnection(SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comand = new SqlCommand();
                    {
                        comand.Connection = conn;
                        comand.CommandText = "SELECT CodigoBarras,PrecioMayoreo,PrecioFrecuente,PrecioPublico,Unidad FROM Productos WHERE Descripcion=@Descripcion";

                    };

                    comand.Parameters.Add("@Descripcion", SqlDbType.VarChar, 100);
                    comand.Parameters["@Descripcion"].Value = dato.sDescripcion;

                    SqlDataReader reader = comand.ExecuteReader();
                    dato.sIDProducto = sID;
                    while (reader.Read())
                    {
                        bExiste = true;
                        dato.sIDProducto = reader[0].ToString();
                        dato.dPrecioMayoreo = Convert.ToDouble(reader[1].ToString());
                        dato.dPrecioFrecuente = Convert.ToDouble(reader[2].ToString());
                        dato.dPrecioPublico = Convert.ToDouble(reader[3].ToString());
                        dato.sUnidad = reader[4].ToString();


                    }
                    conn.Close();
                    bAllok = true;

                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return bAllok;
        }

        public DataTable ConsultarVentasT(String sFolio)
        {
            Boolean bAllok = false;
            DataTable table = new DataTable();
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comando = new SqlCommand();
                    {
                        comando.Connection = conn;
                        comando.CommandText = "SELECT IDProducto,Descripcion,Cantidad,PrecioUnitario,Importe FROM VentasDetalle WHERE Folio=@Folio";

                    };
                    comando.Parameters.Add("@Folio", SqlDbType.VarChar, 10);

                    comando.Parameters["@Folio"].Value =sFolio;

                    SqlDataAdapter adapter = new SqlDataAdapter(comando);
                    adapter.Fill(table);



                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return table;
        }

        public DataTable ConsultarPresupuestoT(String sFolio)
        {
            Boolean bAllok = false;
            DataTable table = new DataTable();
            using (SqlConnection conn = new SqlConnection(OperacionesProductos.SCadenaConexion))
                try
                {
                    conn.Open();
                    SqlCommand comando = new SqlCommand();
                    {
                        comando.Connection = conn;
                        comando.CommandText = "SELECT IDProducto,Descripcion,Cantidad,PrecioUnitario,Importe FROM PresupuestosDetalle WHERE Folio=@Folio";

                    };
                    comando.Parameters.Add("@Folio", SqlDbType.VarChar, 10);

                    comando.Parameters["@Folio"].Value = sFolio;

                    SqlDataAdapter adapter = new SqlDataAdapter(comando);
                    adapter.Fill(table);



                    conn.Close();
                    bAllok = true;
                }
                catch (Exception ex)
                {
                    sLastError = ex.Message;
                }
            return table;
        }
        


    }
}
